<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Traits\CustomSaveTrait;




class Candidate extends Model {
	use CustomSaveTrait;

	protected $guarded = [];

	protected $inputRules = [
		'name' => ['alphaOnly', 'upperFirstWord'],
		'contact' => ['alphaNumericSymbolOnly'],
		'email' => ['emailCharsOnly'],
		'source' => ['alphaNumericSymbolOnly', 'upperFirstWord'],
		'status' => ['alphaOnly', 'upperFirstWord']
	];



	public function jobVacancyApplications() {
		return $this->hasMany('App\JobVacancyApplication', 'candidateId');
	}


	public function networkProfiles() {
		return $this->hasMany('App\CandidateNetworkProfile', 'candidateId');
	}


	public function files() {
		return $this->hasMany('App\CandidateFile', 'candidateId');
	}






	public function childDataSave($input, $fkId) {
		if(isset($input->networkProfiles)) {
			$networkProfile = new CandidateNetworkProfile();
			$networkProfile->customSave($input->networkProfiles, $fkId, 'candidateId');
		}

		if(isset($input->files)) {
			$networkProfile = new CandidateFile();
			$networkProfile->customSave($input->files, $fkId, 'candidateId');
		}

		if(isset($input->jobVacancyApplications)) {
			$jobVacancyApplication = new JobVacancyApplication();
			$jobVacancyApplication->customSave($input->jobVacancyApplications, $fkId, 'candidateId');
		}
	}











	
}
