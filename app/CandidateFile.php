<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Traits\CustomSaveTrait;



class CandidateFile extends Model {
	use CustomSaveTrait;

	protected $table = 'candidate_files';
	protected $guarded = [];
	public $timestamps = false;

	protected $inputRules = [
		'candidateId' => ['numericOnly'],
		'file' => [],
		'date' => ['datetime'],
	];



	public function candidate() {
		return $this->belongsTo('App\Candidate', 'candidateId');
	}




	public function childDataSave($input, $fkId) {
		// no child
	}










}
