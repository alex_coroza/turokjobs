<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Traits\CustomSaveTrait;



class CandidateNetworkProfile extends Model {
	use CustomSaveTrait;

	protected $table = 'candidate_network_profiles';
	protected $guarded = [];
	public $timestamps = false;

	protected $inputRules = [
		'candidateId' => ['numericOnly'],
		'networkName' => ['upperFirstWord', 'alphaNumericSymbolOnly'],
		'link' => []
	];



	public function candidate() {
		return $this->belongsTo('App\Candidate', 'candidateId');
	}




	public function childDataSave($input, $fkId) {
		// no child
	}








}
