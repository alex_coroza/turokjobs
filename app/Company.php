<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


use App\Traits\CustomSaveTrait;





class Company extends Model {
	use CustomSaveTrait;

	protected $guarded = [];

	protected $inputRules = [
		'name' => ['alphaNumericOnly', 'upperFirstWord'],
		'address' => ['alphaNumericSymbolOnly', 'upperFirstWord'],
		'city' => ['alphaNumericSymbolOnly', 'upperFirstWord'],
		'province' => ['alphaNumericSymbolOnly', 'upperFirstWord'],
		'country' => ['alphaNumericSymbolOnly', 'upperFirstWord'],
		'postalCode' => ['numericOnly'],
		'contactNumbers' => ['alphaNumericSymbolOnly'],
		'websites' => [],
		'division' => ['alphaNumericOnly', 'upperFirstWord'],
		'types' => ['alphaNumericSymbolOnly'],
		'pezaRegistered' => ['alphaNumericOnly', 'noSpace'],
		'otherInfo' => ['alphaNumericSymbolOnly', 'spaceTrim']
	];


	public function jobVacancies() {
		return $this->hasMany('App\JobVacancy', 'companyId');
	}


	public function contactPersons() {
		return $this->hasMany('App\CompanyContactPerson', 'companyId');
	}


	public function notes() {
		return $this->hasMany('App\CompanyNote', 'companyId');
	}






	
	public function childDataSave($input, $fkId) {
		if(isset($input->contactPersons)) {
			$contactPerson = new CompanyContactPerson();
			$contactPerson->customSave($input->contactPersons, $fkId, 'companyId');
		}

		if(isset($input->jobVacancies)) {
			$jobVacancy = new JobVacancy();
			$jobVacancy->customSave($input->jobVacancies, $fkId, 'companyId');
		}

		if(isset($input->notes)) {
			$note = new CompanyNote();
			$note->customSave($input->notes, $fkId, 'companyId');
		}
	}



	







	// ACCESSORS
	public function getContactNumbersAttribute($value) {
		return explode('|', $value);
	}

	public function getWebsitesAttribute($value) {
		return explode('|', $value);
	}

	public function getTypesAttribute($value) {
		return explode('|', $value);
	}






	
}
