<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


use App\Traits\CustomSaveTrait;



class CompanyContactPerson extends Model {
	use CustomSaveTrait;
	
	protected $table = 'company_contact_persons';
	protected $guarded = [];
	public $timestamps = false;
	
	protected $inputRules = [
		'companyId' => ['numericOnly'],
		'name' => ['alphaOnly', 'upperFirstWord'],
		'position' => ['alphaNumericOnly', 'upperFirstWord'],
		'contact' => ['alphaNumericSymbolOnly'],
		'email' => ['emailCharsOnly']
	];



	public function company() {
		return $this->belongsTo('App\Company', 'companyId');
	}




	public function childDataSave($input, $fkId) {
		// no child
	}







}
