<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


use App\Traits\CustomSaveTrait;




class CompanyNote extends Model {
	use CustomSaveTrait;
	
	protected $table = 'company_notes';
	protected $guarded = [];
	public $timestamps = false;
	

	protected $inputRules = [
		'companyId' => ['numericOnly'],
		'note' => ['alphaNumericSymbolOnly'],
		'date' => ['date']
	];


	public function company() {
		return $this->belongsTo('App\Company', 'companyId');
	}





	public function childDataSave($input, $fkId) {
		// no child
	}


	







}
