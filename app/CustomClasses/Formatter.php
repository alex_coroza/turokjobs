<?php

namespace App\CustomClasses;



class Formatter {


	/**
	 * trims all whitespaces outside the string then all the consecutive white spaces within the string are converted into a single space 
	 */
	public function spaceTrim($sample) {
		return trim(preg_replace('/\s+/', ' ', $sample));
	}

	// removes all spaces
	public function noSpace($sample) {
		return preg_replace('/\s+/', '', $sample);
	}

	/**
	 * removes non alpha characters in the string
	 */
	public function alphaOnly($sample) {
		return preg_replace('/[^A-Z a-z]/', '', $sample);
	}

	/**
	 * removes non numeric characters in the string
	 */
	public function numericOnly($sample) {
		return preg_replace('/[^0-9]/', '', $sample);
	}

	/**
	 * removes non alphanumeric characters in the string
	 */
	public function alphaNumericOnly($sample) {
		return preg_replace('/[^A-Za-zñÑ 0-9]/', '', $sample);
	}

	public function alphaNumericSymbolOnly($sample) {
		return preg_replace('/[^a-zA-Z0-9ñÑ ,-._#&!?]/', '', $sample);
	}

	public function emailCharsOnly($sample) {
		return preg_replace('/[^A-Za-z 0-9@._]/', '', $sample);
	}


	// returns uppercased first letter per word
	public function upperFirstWord($sample) {
		return ucwords($sample);
	}


	// returns uppercased string
	public function upperCase($sample) {
		return strtoupper($sample);
	}


	// returns lowercased string
	public function lowerCase($sample) {
		return strtolower($sample);
	}


	// returns date in mysql format 
	public function date($sample) {
		if($sample == null) return null;
		else return date('Y-m-d', strtotime($sample));
	}

	// returns datetime in mysql format 
	public function datetime($sample) {
		if($sample == null) return null;
		else return date('Y-m-d H:i:s', strtotime($sample));
	}






	// return formatted string based on requested format functions
	public function multiFormat($sample, $formatFunctions = array()) {
		// gumamet ng foreach para maexecute in order ung mga functions
		foreach ($formatFunctions as $function) {
			if($function == 'spaceTrim') {
				$sample = $this->spaceTrim($sample);
			}

			if($function == 'noSpace') {
				$sample = $this->noSpace($sample);
			}

			if($function == 'alphaOnly') {
				$sample = $this->alphaOnly($sample);
			}

			if($function == 'numericOnly') {
				$sample = $this->numericOnly($sample);
			}

			if($function == 'alphaNumericOnly') {
				$sample = $this->alphaNumericOnly($sample);
			}

			if($function == 'alphaNumericSymbolOnly') {
				$sample = $this->alphaNumericSymbolOnly($sample);
			}

			if($function == 'emailCharsOnly') {
				$sample = $this->emailCharsOnly($sample);
			}

			if($function == 'upperFirstWord') {
				$sample = $this->upperFirstWord($sample);
			}

			if($function == 'upperCase') {
				$sample = $this->upperCase($sample);
			}

			if($function == 'lowerCase') {
				$sample = $this->lowerCase($sample);
			}

			if($function == 'date') {
				$sample = $this->date($sample);
			}

			if($function == 'datetime') {
				$sample = $this->datetime($sample);
			}
		}

		return $sample;
	}






	public function formatDelimitedArrayString($array, $delimiter, $rule) {
		$formattedArray = array();
		foreach ($array as $key => $value) {
			$formattedArray[] = $this->multiFormat($value, $rule);
		}
		return implode($delimiter, $formattedArray);
	}




	public function formatModelInput($input, $rules) {
		$formattedInput = array();

		foreach ($input as $key => $value) {
			if(isset($rules[$key])) {
				if(is_array($value)) {
					$formattedInput[$key] = $this->formatDelimitedArrayString($value, "|", $rules[$key]);
				} else {
					$formattedInput[$key] = $this->multiFormat($value, $rules[$key]);
				}
			}
		}

		return $formattedInput;
	}
















}