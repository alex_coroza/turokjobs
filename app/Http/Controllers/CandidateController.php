<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;


use App\CustomClasses\Utility;
use App\CustomClasses\FileUpload;

use App\Candidate;
use App\CandidateFile;
use App\CandidateNetworkProfile;
use App\JobVacancyApplication;





class CandidateController extends Controller {


	public function enlist() {
		$options = json_decode(file_get_contents("php://input"));
		$candidateOrm = new Candidate();


		// conditions
		if(isset($options->conditions)) {
			foreach ($options->conditions as $condition) {
				$fieldName = $condition[0];
				$operator = isset($condition[2]) ? $condition[1] : '=';
				$value = isset($condition[2]) ? $condition[2] : $condition[1];
				$candidateOrm = $candidateOrm->where($fieldName, $operator, $value);
			}
		}


		// search
		if(isset($options->search)) {
			$options->search = explode(' ', $options->search);

			$candidateOrm = $candidateOrm->where(function($query) use($options) {
				foreach ($options->search as $key => $value) {
					$query->orWhere('name', 'LIKE', '%'.$value.'%');
					$query->orWhere('contact', 'LIKE', '%'.$value.'%');
					$query->orWhere('email', 'LIKE', '%'.$value.'%');
				}
			});
		}


		// orderBy
		if(isset($options->orderBy)) {
			foreach ($options->orderBy as $key => $value) {
				$candidateOrm = $candidateOrm->orderBy($value[0], $value[1]);
			}
		}


		// populate
		if(isset($options->populate)) {
			foreach ($options->populate as $populate) {
				$candidateOrm = $candidateOrm->with($populate);
			}
		}


		// enlist or read
		if(isset($options->id)) {
			$candidateOrm = $candidateOrm->where('id', $options->id)->first();
		} else {
			$candidateOrm = $candidateOrm->get();
		}


		return $candidateOrm;
	}







	public function save() {
		$options = json_decode(file_get_contents("php://input"));
		$utility = new Utility();
		$candidateId = isset($options->info->id) ? $options->info->id:null;

		$candidate = Candidate::firstOrNew(['id' => $candidateId]);
		$candidate->customSave($options->info, $candidateId);

		return $candidate;
	}







	public function delete() {
		$options = json_decode(file_get_contents("php://input"));
		Candidate::destroy($options->id);
	}





	public function uploadFile() {
		$fileName = '('.$_POST['candidate']['id'].')'.$_POST['candidate']['name'].' '.$_FILES['file']['name'];
		$fileUpload = new FileUpload($_FILES['file'], $fileName);
		if(!$fileUpload->checkFileExist()) return 'exists';
		$fileUpload->uploadFile();

		// and also save it to database
		$candidateFile = new CandidateFile();
		$candidateFile->candidateId = $_POST['candidate']['id'];
		$candidateFile->file = $fileName;
		$candidateFile->date = date('Y-m-d H:i:s', time());
		$candidateFile->save();

		return $fileUpload->uploadInfoResponse();
	}




	public function deleteFile() {
		$options = json_decode(file_get_contents("php://input"));
		unlink('files/candidate/'.$options->file->file);

		// and also delete record in database
		CandidateFile::where('id', $options->file->id)->delete();
	}







	
}
