<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\CustomClasses\Utility;

use App\Company;
use App\CompanyBusiness;
use App\CompanyContactPerson;
use App\CompanyNote;




class CompanyController extends Controller {


	public function enlist() {
		$options = json_decode(file_get_contents("php://input"));
		$companyOrm = new Company();


		// conditions
		if(isset($options->conditions)) {
			foreach ($options->conditions as $condition) {
				$fieldName = $condition[0];
				$operator = isset($condition[2]) ? $condition[1] : '=';
				$value = isset($condition[2]) ? $condition[2] : $condition[1];
				$companyOrm = $companyOrm->where($fieldName, $operator, $value);
			}
		}


		// search
		if(isset($options->search)) {
			$options->search = explode(' ', $options->search);

			$companyOrm = $companyOrm->where(function($query) use($options) {
				foreach ($options->search as $key => $value) {
					$query->orWhere('name', 'LIKE', '%'.$value.'%');
					$query->orWhere('address', 'LIKE', '%'.$value.'%');
					$query->orWhere('city', 'LIKE', '%'.$value.'%');
					$query->orWhere('province', 'LIKE', '%'.$value.'%');
					$query->orWhere('country', 'LIKE', '%'.$value.'%');
					$query->orWhere('postalCode', 'LIKE', '%'.$value.'%');
					$query->orWhere('contactNumbers', 'LIKE', '%'.$value.'%');
					$query->orWhere('websites', 'LIKE', '%'.$value.'%');
					$query->orWhere('division', 'LIKE', '%'.$value.'%');
					$query->orWhere('types', 'LIKE', '%'.$value.'%');
					$query->orWhere('otherInfo', 'LIKE', '%'.$value.'%');
				}
			});
		}


		// orderBy
		if(isset($options->orderBy)) {
			foreach ($options->orderBy as $key => $value) {
				$companyOrm = $companyOrm->orderBy($value[0], $value[1]);
			}
		}


		// populate
		if(isset($options->populate)) {
			foreach ($options->populate as $populate) {
				$companyOrm = $companyOrm->with($populate);
			}
		}


		// enlist or read
		if(isset($options->id)) {
			$companyOrm = $companyOrm->where('id', $options->id)->first();
		} else {
			$companyOrm = $companyOrm->get();
		}


		return $companyOrm;
	}







	public function save() {
		$options = json_decode(file_get_contents("php://input"));
		$utility = new Utility();
		$companyId = isset($options->info->id) ? $options->info->id:null;

		$company = Company::firstOrNew(['id' => $companyId]);
		$company->customSave($options->info, $companyId);

		return $company;
	}







	public function delete() {
		$options = json_decode(file_get_contents("php://input"));
		Company::destroy($options->id);
	}




	
}
