<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;


use App\CustomClasses\Utility;


use App\User;
use App\User\UserSession;





class CustomAuthController extends Controller {





	public function login() {
		$options = json_decode(file_get_contents("php://input"));
		$utility = new Utility();



		$user = User::where('email', $options->email)->first();
		$userCount = $this->checkPassword($options->email, $options->password);

		if($userCount > 0) {
			$this->sessionStart($user->id);
			return $user->select('id', 'name', 'email', 'type')->first();
		} else {
			return 'error';
		}
	}




	public function sessionStart($userId) {
		$utility = new Utility();

		// use php sessions instead
		session_start();

		if(isset($_SESSION['loginInfo'])) {
			return;
		}

		$_SESSION['loginInfo']['userId'] = $userId;
		$_SESSION['loginInfo']['lastActionTime'] = time();
	}





	// sessionDestroy
	public function logout() {
		$options = json_decode(file_get_contents("php://input"));
		session_destroy();
	}




	public function loginInfo() {
		$user = User::find($_SESSION['loginInfo']['userId'])->makeHidden(['password', 'salt', 'created_at', 'updated_at']);
		$user->loginInfo = $_SESSION['loginInfo'];
		return $user;
	}





	public function accessCheckPassword() {
		$options = json_decode(file_get_contents("php://input"));
		return $this->checkPassword($options->email, $options->password);
	}
	public function checkPassword($email, $password) {
		$user = User::where('email', $email)->first();
		return User::where('email', $email)->where('password', sha1(md5($password.$user->salt)))->count();
	}







}
