<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\CustomClasses\Utility;

use App\JobVacancyApplication;
use App\JobVacancyApplicationStage;



class JobVacancyApplicationController extends Controller {


	public function enlist() {
		$options = json_decode(file_get_contents("php://input"));
		$jobVacancyApplicationOrm = new JobVacancyApplication();


		// conditions
		if(isset($options->conditions)) {
			foreach ($options->conditions as $condition) {
				$fieldName = $condition[0];
				$operator = isset($condition[2]) ? $condition[1] : '=';
				$value = isset($condition[2]) ? $condition[2] : $condition[1];
				$jobVacancyApplicationOrm = $jobVacancyApplicationOrm->where($fieldName, $operator, $value);
			}
		}



		// search
		if(isset($options->search)) {
			$options->search = explode(' ', $options->search);

			$jobVacancyApplicationOrm = $jobVacancyApplicationOrm->where(function($query) use($options) {
				foreach ($options->search as $key => $value) {
					$query->orWhere('status', 'LIKE', '%'.$value.'%');
					$query->orWhere('dateClosed', 'LIKE', '%'.$value.'%');
				}
			});
		}


		// orderBy
		if(isset($options->orderBy)) {
			foreach ($options->orderBy as $key => $value) {
				$jobVacancyApplicationOrm = $jobVacancyApplicationOrm->orderBy($value[0], $value[1]);
			}
		}



		// populate
		if(isset($options->populate)) {
			foreach ($options->populate as $populate) {
				$jobVacancyApplicationOrm = $jobVacancyApplicationOrm->with($populate);
			}
		}



		// enlist or read
		if(isset($options->id)) {
			$jobVacancyApplicationOrm = $jobVacancyApplicationOrm->where('id', $options->id)->first();
		} else {
			$jobVacancyApplicationOrm = $jobVacancyApplicationOrm->get();
		}


		return $jobVacancyApplicationOrm;
	}







	public function save() {
		$options = json_decode(file_get_contents("php://input"));
		$utility = new Utility();
		$jobVacancyApplicationId = isset($options->info->id) ? $options->info->id:null;

		$jobVacancyApplication = JobVacancyApplication::firstOrNew(['id' => $jobVacancyApplicationId]);
		$jobVacancyApplication->customSave($options->info, $jobVacancyApplicationId);

		return $jobVacancyApplication;
	}







	public function delete() {
		$options = json_decode(file_get_contents("php://input"));
		JobVacancyApplication::destroy($options->id);
	}




}
