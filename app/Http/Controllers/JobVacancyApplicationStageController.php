<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;


use App\JobVacancyApplicationStage;




class JobVacancyApplicationStageController extends Controller {



	public function save() {
		$options = json_decode(file_get_contents("php://input"));
		$jobVacancyApplicationStageId = isset($options->info->id) ? $options->info->id:null;

		$jobVacancy = JobVacancyApplicationStage::firstOrNew(['id' => $jobVacancyApplicationStageId]);
		$jobVacancy->customSave($options->info, $jobVacancyApplicationStageId);

		return $jobVacancy;
	}






	public function delete() {
		$options = json_decode(file_get_contents("php://input"));
		JobVacancyApplicationStage::destroy($options->id);
	}




	
}
