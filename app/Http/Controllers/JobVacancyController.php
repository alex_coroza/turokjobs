<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;


use App\CustomClasses\Utility;

use App\JobVacancy;
use App\JobVacancyApplication;


class JobVacancyController extends Controller {
	





	public function enlist() {
		$options = json_decode(file_get_contents("php://input"));
		$jobVacancyOrm = new JobVacancy();


		// conditions
		if(isset($options->conditions)) {
			foreach ($options->conditions as $condition) {
				$fieldName = $condition[0];
				$operator = isset($condition[2]) ? $condition[1] : '=';
				$value = isset($condition[2]) ? $condition[2] : $condition[1];
				$jobVacancyOrm = $jobVacancyOrm->where($fieldName, $operator, $value);
			}
		}



		// search
		if(isset($options->search)) {
			$options->search = explode(' ', $options->search);

			$jobVacancyOrm = $jobVacancyOrm->where(function($query) use($options) {
				foreach ($options->search as $key => $value) {
					$query->orWhere('title', 'LIKE', '%'.$value.'%');
				}
			});
		}


		// orderBy
		if(isset($options->orderBy)) {
			foreach ($options->orderBy as $key => $value) {
				$jobVacancyOrm = $jobVacancyOrm->orderBy($value[0], $value[1]);
			}
		}


		// populate
		if(isset($options->populate)) {
			foreach ($options->populate as $populate) {
				$jobVacancyOrm = $jobVacancyOrm->with($populate);
			}
		}


		// enlist or read
		if(isset($options->id)) {
			$jobVacancyOrm = $jobVacancyOrm->where('id', $options->id)->first();
		} else {
			$jobVacancyOrm = $jobVacancyOrm->get();
		}


		return $jobVacancyOrm;
	}







	public function save() {
		$options = json_decode(file_get_contents("php://input"));
		$utility = new Utility();
		$jobVacancyId = isset($options->info->id) ? $options->info->id:null;

		$jobVacancy = JobVacancy::firstOrNew(['id' => $jobVacancyId]);
		$jobVacancy->customSave($options->info, $jobVacancyId);

		return $jobVacancy;
	}







	public function delete() {
		$options = json_decode(file_get_contents("php://input"));
		JobVacancy::destroy($options->id);
	}















}
