<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use \DB;



use App\JobVacancyApplicationStage;
use App\JobVacancyApplication;


class MainDashboardController extends Controller {


	// get data for kpi table
	public function kpiCount() {
		$options = json_decode(file_get_contents("php://input"));

		$results = array();

		$results['Job Vacancies'] = JobVacancyApplication::select(DB::raw("COUNT(*) as count"), DB::raw("DATE_FORMAT(date, '%b') AS month"))->whereYear('date', $options->year)->groupBy('month');
		$results['CV Sent'] = JobVacancyApplicationStage::select(DB::raw("COUNT(*) as count"), DB::raw("DATE_FORMAT(date, '%b') AS month"))->whereYear('date', $options->year)->where('stage', 'CV Sent')->groupBy('month');
		$results['Interviews'] = JobVacancyApplicationStage::select(DB::raw("COUNT(*) as count"), DB::raw("DATE_FORMAT(date, '%b') AS month"))->whereYear('date', $options->year)->where('stage', 'LIKE', '%Interview%')->groupBy('month');
		$results['Job Offer'] = JobVacancyApplicationStage::select(DB::raw("COUNT(*) as count"), DB::raw("DATE_FORMAT(date, '%b') AS month"))->whereYear('date', $options->year)->where('stage', 'Job Offer')->groupBy('month');
		$results['Placement'] = JobVacancyApplicationStage::select(DB::raw("COUNT(*) as count"), DB::raw("DATE_FORMAT(date, '%b') AS month"))->whereYear('date', $options->year)->where('stage', 'Placement')->groupBy('month');
		$results['Other'] = JobVacancyApplicationStage::select(DB::raw("COUNT(*) as count"), DB::raw("DATE_FORMAT(date, '%b') AS month"))->whereYear('date', $options->year)->where('stage', 'Other')->groupBy('month');

		// additional condition for recruiter
		if(isset($options->recruiter)) {
			$results['Job Vacancies'] = $results['Job Vacancies']->where('userId', $options->recruiter);
			$results['CV Sent'] = $results['CV Sent']->whereHas('jobVacancyApplication', function($query) use($options) {
				$query->where('userId', $options->recruiter);
			});
			$results['Interviews'] = $results['Interviews']->whereHas('jobVacancyApplication', function($query) use($options) {
				$query->where('userId', $options->recruiter);
			});
			$results['Job Offer'] = $results['Job Offer']->whereHas('jobVacancyApplication', function($query) use($options) {
				$query->where('userId', $options->recruiter);
			});
			$results['Placement'] = $results['Placement']->whereHas('jobVacancyApplication', function($query) use($options) {
				$query->where('userId', $options->recruiter);
			});
			$results['Other'] = $results['Other']->whereHas('jobVacancyApplication', function($query) use($options) {
				$query->where('userId', $options->recruiter);
			});
		}

		// get results
		$results['Job Vacancies'] = $results['Job Vacancies']->get();
		$results['CV Sent'] = $results['CV Sent']->get();
		$results['Interviews'] = $results['Interviews']->get();
		$results['Job Offer'] = $results['Job Offer']->get();
		$results['Placement'] = $results['Placement']->get();
		$results['Other'] = $results['Other']->get();


		return $results;
	}








}
