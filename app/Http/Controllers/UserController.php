<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;


use App\CustomClasses\Utility;

use App\User;





class UserController extends Controller {


	public function enlist() {
		$options = json_decode(file_get_contents("php://input"));
		$userOrm = new User();


		// conditions
		if(isset($options->conditions)) {
			foreach ($options->conditions as $condition) {
				$fieldName = $condition[0];
				$operator = isset($condition[2]) ? $condition[1] : '=';
				$value = isset($condition[2]) ? $condition[2] : $condition[1];
				$userOrm = $userOrm->where($fieldName, $operator, $value);
			}
		}


		// search
		if(isset($options->search)) {
			$options->search = explode(' ', $options->search);

			$userOrm = $userOrm->where(function($query) use($options) {
				foreach ($options->search as $key => $value) {
					$query->orWhere('name', 'LIKE', '%'.$value.'%');
					$query->orWhere('email', 'LIKE', '%'.$value.'%');
					$query->orWhere('type', 'LIKE', '%'.$value.'%');
				}
			});
		}


		// orderBy
		if(isset($options->orderBy)) {
			foreach ($options->orderBy as $key => $value) {
				$userOrm = $userOrm->orderBy($value[0], $value[1]);
			}
		}


		// populate
		if(isset($options->populate)) {
			foreach ($options->populate as $populate) {
				$userOrm = $userOrm->with($populate);
			}
		}


		// enlist or read
		if(isset($options->id)) {
			$userOrm = $userOrm->where('id', $options->id)->first();
		} else {
			$userOrm = $userOrm->get();
		}


		// always hide password and salt
		return $userOrm->makeHidden(['password', 'salt']);
	}






	public function register() {
		$options = json_decode(file_get_contents("php://input"));
		$utility = new Utility();
		$userModel = new User();

		$salt = $utility->generateRandomString(25);
		$password = $utility->generateRandomString(8);

		if($this->checkEmail($options->info->email) > 0) return 'error';

		$options->info->password = sha1(md5($password.$salt));
		$options->info->salt = $salt;
		$userModel->customSave($options->info);

		return $password;
	}







	// save changes in user's record such as password
	public function save() {
		$options = json_decode(file_get_contents("php://input"));
		$utility = new Utility();
		$userId = isset($options->info->id) ? $options->info->id:null;

		$user = User::firstOrNew(['id' => $userId]);
		if($this->checkEmail($options->info->email) > 0 && $user->email != $options->info->email) return 'error';
		if(isset($options->info->newPassword)) {
			$salt = $utility->generateRandomString(25);
			$options->info->password = sha1(md5($options->info->newPassword.$salt));
			$options->info->salt = $salt;
		}
		$user->customSave($options->info, $userId);

		return $user;
	}






	public function resetPassword() {
		$options = json_decode(file_get_contents("php://input"));
		$utility = new Utility();

		$salt = $utility->generateRandomString(25);
		$password = $utility->generateRandomString(8);

		$user = User::find($options->userId);
		$user->password = sha1(md5($password.$salt));
		$user->salt = $salt;
		$user->save();
		return $password;
	}








	public function accessCheckEmail() {
		$options = json_decode(file_get_contents("php://input"));
		return $this->checkEmail($options->email);
	}
	public function checkEmail($email) {
		return User::where('email', $email)->count();
	}












	
}
