<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


use App\Traits\CustomSaveTrait;



class JobVacancy extends Model {
	use CustomSaveTrait;

	protected $guarded = [];

	protected $inputRules = [
		'companyId' => ['numericOnly'],
		'title' => ['alphaNumericOnly', 'upperFirstWord'],
		'slots' => ['numericOnly'],
		'requirements' => ['alphaNumericSymbolOnly'],
		'overview' => ['alphaNumericSymbolOnly'],
		'description' => ['alphaNumericSymbolOnly'],
		'positionLevel' => ['alphaNumericSymbolOnly', 'upperFirstWord'],
		'priorityLevel' => ['numericOnly'],
		'salaryRange' => ['numericOnly'],
		'placementPercent' => ['numericOnly'],
		'status' => ['alphaNumericOnly'],
		'dateClosed' => ['date'],
		'created_at' => ['date'],
		'remarks' => ['alphaNumericSymbolOnly']
	];

	

	public function applications() {
		return $this->hasMany('App\JobVacancyApplication', 'jobVacancyId');
	}
	

	public function company() {
		return $this->belongsTo('App\Company', 'companyId');
	}








	public function childDataSave($input, $fkId) {
		if(isset($input->applications)) {
			$jobVacancyApplication = new JobVacancyApplication();
			$jobVacancyApplication->customSave($input->applications, $fkId, 'jobVacancyId');
		}
	}












	



	// ACCESSORS
	public function getRequirementsAttribute($value) {
		return explode('|', $value);
	}






	
}
