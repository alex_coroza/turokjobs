<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


use App\Traits\CustomSaveTrait;





class JobVacancyApplication extends Model {
	use CustomSaveTrait;
	
	protected $table = 'job_vacancy_applications';
	protected $guarded = [];
	public $timestamps = false;

	protected $inputRules = [
		'userId' => ['numericOnly'],
		'jobVacancyId' => ['numericOnly'],
		'candidateId' => ['numericOnly'],
		'date' => ['datetime'],
		'dateClosed' => ['date'],
		'status' => ['alphaNumericSymbolOnly']
	];




	public function jobVacancy() {
		return $this->belongsTo('App\JobVacancy', 'jobVacancyId');
	}


	public function candidate() {
		return $this->belongsTo('App\Candidate', 'candidateId');
	}


	public function stages() {
		return $this->hasMany('App\JobVacancyApplicationStage', 'jobVacancyApplicationId');
	}



	public function user() {
		return $this->belongsTo('App\User', 'userId');
	}









	

	public function childDataSave($input, $fkId) {
		if(isset($input->stages)) {
			$jobVacancyApplicationStage = new JobVacancyApplicationStage();
			$jobVacancyApplicationStage->customSave($input->stages, $fkId, 'jobVacancyApplicationId');
		}
	}












}
