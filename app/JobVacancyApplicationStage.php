<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


use App\Traits\CustomSaveTrait;



class JobVacancyApplicationStage extends Model {
	use CustomSaveTrait;

	protected $table = 'job_vacancy_application_stages';
	protected $guarded = [];
	public $timestamps = false;

	protected $inputRules = [
		'jobVacancyApplicationId' => ['numericOnly'],
		'date' => ['date'],
		'stage' => ['alphaNumericSymbolOnly'],
		'result' => ['alphaNumericSymbolOnly'],
		'notes' => ['alphaNumericSymbolOnly']
	];




	public function jobVacancyApplication() {
		return $this->belongsTo('App\JobVacancyApplication', 'jobVacancyApplicationId');
	}





	public function childDataSave($input, $fkId) {
		// no child
	}





	



	
}
