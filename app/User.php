<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


use App\Traits\CustomSaveTrait;




class User extends Model {
	use CustomSaveTrait;
	
	protected $guarded = [];

	protected $inputRules = [
		'name' => ['alphaOnly', 'upperFirstWord'],
		'email' => ['emailCharsOnly'],
		'type' => ['alphaOnly', 'upperFirstWord'],
		'features' => [],
		'password' => [],
		'salt' => []
	];




	public function applications() {
		return $this->hasMany('App\JobVacancyApplication', 'userId');
	}





	public function childDataSave($input, $fkId) {
		// no child
	}




	// ACCESSORS
	public function getFeaturesAttribute($value) {
		return explode('|', $value);
	}



}
