/* File: gulpfile.js */

// grab our gulp packages
var gulp  = require('gulp');
var gutil = require('gulp-util');
var jshint = require('gulp-jshint');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var sass = require('gulp-sass');


gulp.task('default', function() {
	console.log('Ang pogi ko!');
});









// compile scss files for guest.css
gulp.task('compileGuestCss', function() {
	gulp.src(['public/assets/css/sass/guest/*.scss', 'public/assets/css/sass/core/core.scss'])
		.pipe(sass())
		.pipe(concat('guest.css'))
		.pipe(gulp.dest('public/assets/css/'));;
});




// compile scss files for admin.css
gulp.task('compileAdminCss', function() {
	gulp.src(['public/assets/css/sass/admin/*.scss', 'public/assets/css/sass/core/core.scss'])
		.pipe(sass())
		.pipe(concat('admin.css'))
		.pipe(gulp.dest('public/assets/css/'));;
});






// WATCH

// auto compile sass on saving
gulp.task('watchCompileScss', function() {
	gulp.watch('public/assets/css/sass/**/*.scss', ['compileGuestCss', 'compileAdminCss']);
});


