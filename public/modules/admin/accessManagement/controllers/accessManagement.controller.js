angular.module('accessManagementModule')
.controller('accessManagementController', ['$scope', '$rootScope', '$http', '$state', '$stateParams', '$uibModalInstance', 'utilityFunctions', 'regexPattern', 'httpFeature', 'httpUser', 'user', 'recruiter', function($scope, $rootScope, $http, $state, $stateParams, $uibModalInstance, utilityFunctions, regexPattern, httpFeature, httpUser, user, recruiter) {
	console.log('accessManagementController initialized!');

	
	$scope.utilityFunctions = utilityFunctions;
	$scope.user = user;
	$scope.recruiter = recruiter;
	$scope.features = [];
	$scope.selectedFeatures = [];



	var loadFeatures = function() {
		$http.post(httpFeature.enlist, {})
		.success(function(data) {
			$scope.features = data;
		});
	};



	$scope.saveSelection = function() {
		// selectedFeatures but removed all false entries
		$scope.recruiter.features = [];

		angular.forEach($scope.selectedFeatures, function(value, key) {
			if(value == true) $scope.recruiter.features.push($scope.features[key].feature);
		});

		$http.post(httpUser.save, { info: $scope.recruiter })
		.success(function(data) {
			$uibModalInstance.close();
		});
	};



	$scope.dismiss = function() {
		$uibModalInstance.dismiss();
	};





	loadFeatures();



}]);