angular.module('accessManagementModule')
.factory('accessManagementServices', ['$q', '$state', '$uibModal', function($q, $state, $uibModal) {
	

	this.showAccessManagement = function(recruiter, user) {
		var deferred = $q.defer();

		$uibModal.open({
			size: 'lg',
			templateUrl: '/modules/admin/accessManagement/views/accessManagement.tpl.html',
			controller: 'accessManagementController',
			resolve: {
				user: function() {
					return user;
				},
				recruiter: function() {
					return recruiter;
				}
			}
		}).result.then(function(response) {
			deferred.resolve(response);
		});

		return deferred.promise;
	};









	return this;

}]);





