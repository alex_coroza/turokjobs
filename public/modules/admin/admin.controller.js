angular.module('adminModule')
.controller('adminController', ['$scope', '$rootScope', '$http', '$window', '$state', '$uibModal', 'utilityFunctions', 'httpCustomAuth', 'user', function($scope, $rootScope, $http, $window, $state, $uibModal, utilityFunctions, httpCustomAuth, user) {
	console.log('adminController initialized!');

	$rootScope.loadingProperties = {
		promise: null,
	};

	$scope.user = user;
	$scope.activeLink = 'admin.dashboard';
	$scope.isNavCollapsed = ($window.innerWidth <= 992) ? true : false;

	$scope.$on('activeLinkUpdate', function(event, args) {
		$scope.activeLink = args.value;
	});

	
	$scope.utilityFunctions = utilityFunctions;

	
	$scope.changeState = function(state) {
		$scope.activeLink = state;
		$state.go(state);
	};




	$scope.home = function() {
		window.location = '/admin';
	};





	$scope.logout = function() {
		$rootScope.loadingProperties.message = 'Logging out...';
		$rootScope.loadingProperties.promise = $http.post(httpCustomAuth.logout, {})
		.success(function() {
			window.location.href = '/';
		});
	};








}]);