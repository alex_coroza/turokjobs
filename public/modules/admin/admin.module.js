angular.module('adminModule', [
	'ngMessages', 'ngAnimate', 'ui.router', 'ui.bootstrap',
	'angularMoment', 'ngFileUpload', 'cgBusy',
	'utility.service', 'http.service', 'angularUiMods.directive',
	'dashboardModule', 'jobVacancyModule', 'jobVacancyApplicationModule', 'jobVacancyApplicationStageModule', 'candidateModule', 'companyModule', 'recruiterModule', 'profileModule', 'accessManagementModule'
]);



// angular-busy override settings
angular.module('adminModule')
.value('cgBusyDefaults', {
	message:'Loading...',
	backdrop: true,
	// templateUrl: 'my_custom_template.html',
	delay: 0,
	minDuration: 1000,
	// wrapperClass: 'my-class my-class2'
});