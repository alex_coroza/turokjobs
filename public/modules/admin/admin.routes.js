angular.module('adminModule')
.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
	$stateProvider
		.state('notFound', {
			url: 'not-found',
			templateUrl: '/modules/core/views/custom404.view.html'
		})
		.state('admin', {
			url: '/',
			templateUrl: '/modules/admin/admin.view.html',
			controller: 'adminController',
			resolve: {
				user: ['$q', '$http', 'httpCustomAuth', function($q, $http, httpCustomAuth) {
					var deferred = $q.defer();
					$http.post(httpCustomAuth.loginInfo, {})
					.success(function(data, status) {
						deferred.resolve(data)
					});
					return deferred.promise;
				}]
			}
		})
	; /*$stateProvider closing*/
}]);
