angular.module('candidateModule')
.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
	$stateProvider
		.state('admin.candidateList', {
			url: 'candidate-list/',
			templateUrl: '/modules/admin/candidate/views/candidateList.view.html',
			controller: 'candidateListController'
		})
		.state('admin.candidateInfo', {
			url: 'candidate-info/{id}',
			templateUrl: '/modules/admin/candidate/views/candidateInfo.view.html',
			controller: 'candidateInfoController'
		})
		.state('admin.candidateEdit', {
			url: 'candidate-edit/{id}',
			templateUrl: '/modules/admin/candidate/views/candidateEdit.view.html',
			controller: 'candidateEditController'
		})
		.state('admin.candidateAdd', {
			url: 'candidate-add/',
			templateUrl: '/modules/admin/candidate/views/candidateAdd.view.html',
			controller: 'candidateAddController'
		})
	; /*$stateProvider closing*/
}]);