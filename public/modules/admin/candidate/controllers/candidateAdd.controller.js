angular.module('candidateModule')
.controller('candidateAddController', ['$scope', '$rootScope', '$http', '$state', '$stateParams', 'utilityFunctions', 'regexPattern', 'httpCandidate', 'httpJobVacancy', 'user', function($scope, $rootScope, $http, $state, $stateParams, utilityFunctions, regexPattern, httpCandidate, httpJobVacancy, user) {
	console.log('candidateAddController initialized!');

	if(!utilityFunctions.inArray('Candidates >> Add', $scope.user.features)) {
		$state.go('admin.profileInfo');
		return;
	}


	$scope.$emit('activeLinkUpdate', { value: 'admin.candidate' });

	$scope.alpha = regexPattern.alpha();
	$scope.alphanumeric = regexPattern.alphanumeric();
	$scope.numeric = regexPattern.numeric();
	$scope.alphanumericSymbol = regexPattern.alphanumericSymbol();


	$scope.user = user;
	$scope.jobVacancies = [];

	
	// default values
	$scope.candidate = {
		networkProfiles: [
			{ networkName: '', link: '' }
		],
		jobVacancyApplications: [
			{ jobVacancyId: '', remarks: '', status: 'Pending' }
		]
	};
	



	$scope.loadJobVacancies = function() {
		var options = {
			populate: ['company']
		};

		$rootScope.loadingProperties.message = 'Loading job vacancies...';
		$rootScope.loadingProperties.promise = $http.post(httpJobVacancy.enlist, options)
		.success(function(data) {
			$scope.jobVacancies = data;
		});
	};






	$scope.submitAdd = function(form) {
		// check for errors first
		// set all fields to "touched"
		angular.forEach(form.$error, function(field) {
			angular.forEach(field, function(errorField) {
				errorField.$setTouched();
			});
		});


		// alert error message if errors detected
		// else submit the form
		if(!utilityFunctions.isEmptyObject(form.$error)) {
			alert('See and fix the errors to save changes!');
		} else {
			if($scope.candidate.networkProfiles.length == 0) delete $scope.candidate.networkProfiles;
			if($scope.candidate.jobVacancyApplications.length == 0) delete $scope.candidate.jobVacancyApplications;

			$rootScope.loadingProperties.message = 'Adding candidate...';
			$rootScope.loadingProperties.promise = $http.post(httpCandidate.save, { info: $scope.candidate })
			.success(function(data) {
				$state.go('admin.candidateList');
			});
		}

	};



	$scope.backToCandidateList = function() {
		var backConfirm = confirm('Discard changes and go back to candidates?');
		if(backConfirm) $state.go('admin.candidateList');
	};



	// function for add/remove field
	$scope.addNetworkProfileField = function() {
		$scope.candidate.networkProfiles.push({ networkName: '', link: '' });
	};
	$scope.removeNetworkProfileField = function() {
		$scope.candidate.networkProfiles.splice(($scope.candidate.networkProfiles.length - 1), 1);
	};

	$scope.addJobVacancyApplicationField = function() {
		$scope.candidate.jobVacancyApplications.push({ jobVacancyId: '', remarks: '', status: 'Pending' });
	};
	$scope.removeJobVacancyApplicationField = function() {
		$scope.candidate.jobVacancyApplications.splice(($scope.candidate.jobVacancyApplications.length - 1), 1);
	};





	$scope.loadJobVacancies();




}]);