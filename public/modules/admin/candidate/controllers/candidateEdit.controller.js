angular.module('candidateModule')
.controller('candidateEditController', ['$scope', '$rootScope', '$http', '$state', '$stateParams', 'utilityFunctions', 'regexPattern', 'httpCandidate', 'httpJobVacancy', function($scope, $rootScope, $http, $state, $stateParams, utilityFunctions, regexPattern, httpCandidate, httpJobVacancy) {
	console.log('candidateEditController initialized!');

	if(!utilityFunctions.inArray('Candidates >> Edit', $scope.user.features)) {
		$state.go('admin.profileInfo');
		return;
	}


	$scope.alpha = regexPattern.alpha();
	$scope.alphanumeric = regexPattern.alphanumeric();
	$scope.numeric = regexPattern.numeric();
	$scope.alphanumericSymbol = regexPattern.alphanumericSymbol();

	
	$scope.candidateId = $state.params.id;
	$scope.candidate = {};
	$scope.currentCandidate = {};
	$scope.jobVacancies = [];



	$rootScope.loadingProperties.message = 'Loading candidate...';
	$rootScope.loadingProperties.promise = $http.post(httpCandidate.enlist, { id: $scope.candidateId, populate: ['networkProfiles', 'jobVacancyApplications.jobVacancy.company'] })
	.success(function(data) {
		$scope.candidate = data;
		$scope.currentCandidate = angular.copy(data);
	});




	$scope.submitEdit = function(form) {
		// check for errors first
		// set all fields to "touched"
		angular.forEach(form.$error, function(field) {
			angular.forEach(field, function(errorField) {
				errorField.$setTouched();
			});
		});


		// alert error message if errors detected
		// else submit the form
		if(!utilityFunctions.isEmptyObject(form.$error)) {
			alert('See and fix the errors to save changes!');
		} else {
			$rootScope.loadingProperties.message = 'Saving changes...';
			$rootScope.loadingProperties.promise = $http.post(httpCandidate.save, { info: $scope.candidate })
			.success(function(data) {
				$state.go('admin.candidateInfo', { id: $scope.candidateId });
			});
		}

	};




	$scope.loadJobVacancies = function() {
		$rootScope.loadingProperties.promise = $http.post(httpJobVacancy.enlist, { populate: ['company'] })
		.success(function(data) {
			$scope.jobVacancies = data;
		});
	};


	
	$scope.backToCandidateInfo = function() {
		var discardConfirm = confirm('Cancel and discard changes?');
		if(discardConfirm) $state.go('admin.candidateInfo', { id: $scope.candidateId });
	};




	// function for add/remove field
	$scope.addNetworkProfileField = function() {
		$scope.candidate.networkProfiles.push({ networkName: '', link: '' });
	};
	$scope.removeNetworkProfileField = function() {
		$scope.candidate.networkProfiles.splice(($scope.candidate.networkProfiles.length - 1), 1);
	};

	$scope.addJobVacancyApplicationField = function() {
		$scope.candidate.jobVacancyApplications.push({ jobVacancyId: '', remarks: '', status: 'Pending' });
	};
	$scope.removeJobVacancyApplicationField = function() {
		$scope.candidate.jobVacancyApplications.splice(($scope.candidate.jobVacancyApplications.length - 1), 1);
	};




	$scope.loadJobVacancies();




}]);