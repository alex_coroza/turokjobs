angular.module('candidateModule')
.controller('candidateInfoController', ['$scope', '$rootScope', '$http', '$timeout', '$state', '$stateParams', '$uibModal', 'Upload', 'utilityFunctions', 'httpCandidate', 'httpJobVacancyApplicationStage', 'jobVacancyApplicationServices', 'jobVacancyApplicationStageServices', function($scope, $rootScope, $http, $timeout, $state, $stateParams, $uibModal, Upload, utilityFunctions, httpCandidate, httpJobVacancyApplicationStage, jobVacancyApplicationServices, jobVacancyApplicationStageServices) {
	console.log('candidateInfoController initialized!');


	if(!utilityFunctions.inArray('Candidates >> Info', $scope.user.features)) {
		$state.go('admin.profileInfo');
		return;
	}


	$scope.$emit('activeLinkUpdate', { value: 'admin.candidate' });

	
	$scope.candidateId = $state.params.id;
	$scope.candidate = {};
	$scope.uploadInfo = {
		uploading: false,
		progress: 0,
		fileOnUpload: ''
	};



	$scope.loadCandidate = function() {
		$rootScope.loadingProperties.message = 'Loading candidate info...';
		$rootScope.loadingProperties.promise = $http.post(httpCandidate.enlist, { id: $scope.candidateId, populate:['jobVacancyApplications.stages', 'jobVacancyApplications.user', 'jobVacancyApplications.jobVacancy.company', 'networkProfiles', 'files'] })
		.success(function(data) {
			$scope.candidate = data;
		});
	};





	$scope.showAddJobApplication = function() {
		jobVacancyApplicationServices.showAddJobApplication($scope.candidate, $scope.user).then(function(response) {
			$scope.loadCandidate();
		});
	};



	$scope.showEditJobApplication = function(application) {
		jobVacancyApplicationServices.showEditJobApplication(application, $scope.user).then(function(response) {
			$scope.loadCandidate();
		});
	};




	$scope.showAddApplicationStage = function(jobVacancyApplication) {
		jobVacancyApplicationStageServices.showAddApplicationStage(jobVacancyApplication, $scope.user).then(function(response) {
			$scope.loadCandidate();
		});
	};


	$scope.applicationStageEdit = function(jobVacancyApplicationStage) {
		jobVacancyApplicationStageServices.showEditApplicationStage(jobVacancyApplicationStage, $scope.user).then(function(response) {
			$scope.loadCandidate();
		});
	};


	$scope.applicationStageDelete = function(applicationStage) {
		var deleteConfirm = confirm('Delete this process?');

		if(deleteConfirm) {
			$rootScope.loadingProperties.message = 'Deleting candidate...';
			$rootScope.loadingProperties.promise = $http.post(httpJobVacancyApplicationStage.delete, { id: applicationStage.id })
			.success(function(data) {
				$scope.loadCandidate();
			});
		}
	};






	$scope.showCandidateEdit = function() {
		$state.go('admin.candidateEdit', { id: $scope.candidateId });
	};



	$scope.backToCandidateList = function() {
		$state.go('admin.candidateList');
	};





	$scope.uploadFile = function(file) {
		if(!file) return;

		$scope.uploadInfo.uploading = true;
		$scope.uploadInfo.fileOnUpload = file.name;

		file.upload = Upload.upload({
			url: httpCandidate.uploadFile,
			data: { file: file, candidate: $scope.candidate }
		});

		file.upload.then(function(response) {
			$scope.uploadInfo.progress = 0;
			$scope.uploadInfo.uploading = false;

			if(response.data == 'exists') {
				alert('File already exists! Please rename the file to be uploaded or delete/rename the existing file in server.');
			} else {
				alert('Upload successful!');
				$scope.loadCandidate();
			}
		}, function(response) {
			// action kapag may server error
		}, function(event) {
			$scope.uploadInfo.progress = Math.min(100, parseInt(100.0 * event.loaded / event.total));
		});
	};




	$scope.fileDelete = function(file) {
		var deleteConfirm = confirm('Delete this file?');

		if(deleteConfirm) {
			$rootScope.loadingProperties.message = 'Deleting file...';
			$rootScope.loadingProperties.promise = $http.post(httpCandidate.deleteFile, { file: file })
			.success(function(data) {
				$scope.loadCandidate();
			});
		}
	};




	$scope.fileDownload =function(file) {
		window.open('/files/candidate/'+file.file);
	};















	$scope.loadCandidate();





}]);