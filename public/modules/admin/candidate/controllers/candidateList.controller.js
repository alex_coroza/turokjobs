angular.module('candidateModule')
.controller('candidateListController', ['$scope', '$rootScope', '$http', '$state', '$stateParams', '$uibModal', 'utilityFunctions', 'httpCandidate', function($scope, $rootScope, $http, $state, $stateParams, $uibModal, utilityFunctions, httpCandidate) {
	console.log('candidateListController initialized!');

	$scope.utilityFunctions = utilityFunctions;

	if(!utilityFunctions.inArray('Candidates', $scope.user.features)) {
		$state.go('admin.profileInfo');
		return;
	}


	$scope.$emit('activeLinkUpdate', { value: 'admin.candidate' });


	$scope.candidates = [];


	$scope.options = {
		search: '',
		orderBy: [
			['created_at', 'DESC']
		]
	};




	$scope.loadCandidates = function() {
		var processedOptions = angular.copy($scope.options);
		if(processedOptions.search == '') delete processedOptions.search;

		$rootScope.loadingProperties.message = 'Loading candidates...';
		$rootScope.loadingProperties.promise = $http.post(httpCandidate.enlist, processedOptions)
		.success(function(data) {
			$scope.candidates = data;
		});
	};




	$scope.showCandidateInfo = function(candidateId) {
		$state.go('admin.candidateInfo', { id: candidateId });
	};



	$scope.showAddCandidate = function() {
		$state.go('admin.candidateAdd');
	};



	$scope.showCandidateEdit = function(candidateId) {
		$state.go('admin.candidateEdit', { id: candidateId });
	};










	





	$scope.loadCandidates();



}]);