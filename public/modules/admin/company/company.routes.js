angular.module('companyModule')
.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
	$urlRouterProvider.when('', '/dashboard/');
	$stateProvider
		.state('admin.companyList', {
			url: 'companyList/',
			templateUrl: '/modules/admin/company/views/companyList.view.html',
			controller: 'companyListController'
		})
		.state('admin.companyAdd', {
			url: 'companyAdd/',
			templateUrl: '/modules/admin/company/views/companyAdd.view.html',
			controller: 'companyAddController'
		})
		.state('admin.companyEdit', {
			url: 'companyEdit/{id}',
			templateUrl: '/modules/admin/company/views/companyEdit.view.html',
			controller: 'companyEditController'
		})
		.state('admin.companyInfo', {
			url: 'companyInfo/{id}',
			templateUrl: '/modules/admin/company/views/companyInfo.view.html',
			controller: 'companyInfoController'
		})
	; /*$stateProvider closing*/
}]);