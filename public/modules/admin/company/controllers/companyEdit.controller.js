angular.module('companyModule')
.controller('companyEditController', ['$scope', '$rootScope', '$http', '$state', '$stateParams', 'utilityFunctions', 'regexPattern', 'httpCompany', function($scope, $rootScope, $http, $state, $stateParams, utilityFunctions, regexPattern, httpCompany) {
	console.log('companyEditController initialized!');

	if(!utilityFunctions.inArray('Companies >> Edit', $scope.user.features)) {
		$state.go('admin.profileInfo');
		return;
	}


	$scope.$emit('activeLinkUpdate', { value: 'admin.company' });
	

	$scope.alpha = regexPattern.alpha();
	$scope.alphanumeric = regexPattern.alphanumeric();
	$scope.numeric = regexPattern.numeric();
	$scope.alphanumericSymbol = regexPattern.alphanumericSymbol();



	$scope.companyId = $state.params.id;
	$scope.company = {};




	$scope.loadCompany = function() {
		$rootScope.loadingProperties.message = 'Loading company...';
		$rootScope.loadingProperties.promise = $http.post(httpCompany.enlist, { id: $scope.companyId, populate: ['contactPersons', 'notes'] })
		.success(function(data) {
			$scope.company = data;
		});
	};

	


	$scope.submitEdit = function(form) {
		// check for errors first
		// set all fields to "touched"
		angular.forEach(form.$error, function(field) {
			angular.forEach(field, function(errorField) {
				errorField.$setTouched();
			});
		});


		// alert error message if errors detected
		// else submit the form
		if(!utilityFunctions.isEmptyObject(form.$error)) {
			alert('See and fix the errors to save changes!');
		} else {
			if($scope.company.contactPersons.length == 0) delete $scope.company.contactPersons;
			if($scope.company.notes.length == 0) delete $scope.company.notes;

			$http.post(httpCompany.save, { info: $scope.company })
			.success(function(data) {
				$state.go('admin.companyList');
			});
		}

	};





	// function for add/remove field
	$scope.addContactNumberField = function() {
		$scope.company.contactNumbers.push('');
	};
	$scope.removeContactNumberField = function() {
		$scope.company.contactNumbers.splice(($scope.company.contactNumbers.length - 1), 1);
	};

	$scope.addWebsiteField = function() {
		$scope.company.websites.push('');
	};
	$scope.removeWebsiteField = function() {
		$scope.company.websites.splice(($scope.company.websites.length - 1), 1);
	};

	$scope.addTypeField = function() {
		$scope.company.types.push('');
	};
	$scope.removeTypeField = function() {
		$scope.company.types.splice(($scope.company.types.length - 1), 1);
	};

	$scope.addContactPersonField = function() {
		$scope.company.contactPersons.push({ name:'', position:'', contact:'', email:'' });
	};
	$scope.removeContactPersonField = function() {
		$scope.company.contactPersons.splice(($scope.company.contactPersons.length - 1), 1);
	};

	$scope.addNoteField = function() {
		$scope.company.notes.push({ name:'', date:new Date() });
	};
	$scope.removeNoteField = function() {
		$scope.company.notes.splice(($scope.company.notes.length - 1), 1);
	};




	

	$scope.backToCompanyList = function() {
		var discardConfirm = confirm('Discard changes and go back to company list?');
		if(discardConfirm) $state.go('admin.companyList');
	};




	$scope.loadCompany();





}]);