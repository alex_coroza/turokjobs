angular.module('companyModule')
.controller('companyInfoController', ['$scope', '$rootScope', '$http', '$state', '$stateParams', 'utilityFunctions', 'httpCompany', function($scope, $rootScope, $http, $state, $stateParams, utilityFunctions, httpCompany) {
	console.log('companyInfoController initialized!');

	$scope.utilityFunctions = utilityFunctions;

	if(!utilityFunctions.inArray('Companies >> Info', $scope.user.features)) {
		$state.go('admin.profileInfo');
		return;
	}


	$scope.$emit('activeLinkUpdate', { value: 'admin.company' });
	


	$scope.companyId = $state.params.id;
	$scope.company = {};




	$scope.loadCompany = function() {
		$rootScope.loadingProperties.message = 'Loading company...';
		$rootScope.loadingProperties.promise = $http.post(httpCompany.enlist, { id: $scope.companyId, populate: ['contactPersons', 'notes', 'jobVacancies'] })
		.success(function(data) {
			$scope.company = data;
		});
	};

	



	$scope.showCompanyEdit = function() {
		$state.go('admin.companyEdit', { id: $scope.companyId });
	};




	

	$scope.backToCompanyList = function() {
		var discardConfirm = confirm('Discard changes and go back to company list?');
		if(discardConfirm) $state.go('admin.companyList');
	};




	$scope.loadCompany();





}]);