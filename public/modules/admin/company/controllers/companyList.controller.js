angular.module('companyModule')
.controller('companyListController', ['$scope', '$rootScope', '$http', '$state', '$stateParams', 'utilityFunctions', 'httpCompany', function($scope, $rootScope, $http, $state, $stateParams, utilityFunctions, httpCompany) {
	console.log('companyListController initialized!');

	$scope.utilityFunctions = utilityFunctions;

	if(!utilityFunctions.inArray('Companies', $scope.user.features)) {
		$state.go('admin.profileInfo');
		return;
	}


	$scope.$emit('activeLinkUpdate', { value: 'admin.company' });
	

	$scope.companies = [];
	$scope.options = {
		search: '',
		populate: ['contactPersons', 'notes']
	};


	

	$scope.loadCompanies = function() {
		$rootScope.loadingProperties.message = 'Loading companies...';
		$rootScope.loadingProperties.promise = $http.post(httpCompany.enlist, $scope.options)
		.success(function(data) {
			$scope.companies = data;
		});
	};



	$scope.showCompanyInfo = function(companyId) {
		$state.go('admin.companyInfo', { id: companyId });
	};



	$scope.showAddCompany = function() {
		$state.go('admin.companyAdd');
	};



	$scope.showCompanyEdit = function(companyId) {
		$state.go('admin.companyEdit', { id: companyId });
	};







	$scope.loadCompanies();


}]);