angular.module('dashboardModule')
.controller('dashboardController', ['$scope', '$rootScope', '$http', '$state', '$stateParams', '$uibModal', 'utilityFunctions', 'httpMainDashboard', 'httpUser', function($scope, $rootScope, $http, $state, $stateParams, $uibModal, utilityFunctions, httpMainDashboard, httpUser) {
	console.log('dashboardController initialized!');

	$scope.utilityFunctions = utilityFunctions;

	if(!utilityFunctions.inArray('Main Dashboard', $scope.user.features)) {
		$state.go('admin.profileInfo');
		return;
	}


	$scope.$emit('activeLinkUpdate', { value: $state.current.name });


	$scope.kpiYear = moment().year().toString();
	$scope.recruiter = ($scope.user.type != 'Admin') ? $scope.user.id:'all';
	$scope.months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
	$scope.data = {};
	$scope.recruiters = [];


	$scope.loadKpiCount = function() {
		var options = { year: $scope.kpiYear };
		if($scope.recruiter != 'all') options.recruiter = $scope.recruiter;

		$rootScope.loadingProperties.promise = $http.post(httpMainDashboard.kpiCount, options)
		.success(function(data) {
			// compute for total and fill missing data with zeroes
			angular.forEach(data, function(value, description) {
				var descriptionData = { total: 0 };
				angular.forEach($scope.months, function(month, key) {
					descriptionData[month] = { month: month, count: 0 };
					angular.forEach(value, function(value2, key) {
						if(month == value2.month) {
							descriptionData[month] = value2;
							descriptionData.total += value2.count;
						}
					});
				});
				$scope.data[description] = descriptionData;
			});
			// operation complete
		});
	};




	$scope.loadRecruiters = function() {
		$rootScope.loadingProperties.promise = $http.post(httpUser.enlist, {})
		.success(function(data) {
			$scope.recruiters = data;
		});
	};





	$scope.loadKpiCount();
	if(utilityFunctions.inArray('Main Dashboard >> Select Recruiter', $scope.user.features)) $scope.loadRecruiters();




}]);