angular.module('dashboardModule')
.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
	$urlRouterProvider.when('', '/dashboard/');
	$stateProvider
		.state('admin.dashboard', {
			url: 'dashboard/',
			templateUrl: '/modules/admin/dashboard/views/dashboard.view.html',
			controller: 'dashboardController'
		})
	; /*$stateProvider closing*/
}]);