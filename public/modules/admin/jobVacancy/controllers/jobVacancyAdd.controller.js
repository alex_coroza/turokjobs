angular.module('jobVacancyModule')
.controller('jobVacancyAddController', ['$scope', '$rootScope', '$http', '$state', '$stateParams', 'utilityFunctions', 'regexPattern', 'httpJobVacancy', 'httpCompany', 'user', function($scope, $rootScope, $http, $state, $stateParams, utilityFunctions, regexPattern, httpJobVacancy, httpCompany, user) {
	console.log('jobVacancyAddController initialized!');

	$scope.utilityFunctions = utilityFunctions;

	if(!utilityFunctions.inArray('Job Vacancies >> Add', $scope.user.features)) {
		$state.go('admin.profileInfo');
		return;
	}
	

	$scope.$emit('activeLinkUpdate', { value: 'admin.jobVacancy' });

	$scope.alpha = regexPattern.alpha();
	$scope.alphanumeric = regexPattern.alphanumeric();
	$scope.numeric = regexPattern.numeric();
	$scope.alphanumericSymbol = regexPattern.alphanumericSymbol();


	$scope.user = user;
	$scope.companies = [];

	
	// default values
	$scope.jobVacancy = {
		status: 'Open',
		requirements: ['']
	};
	



	$scope.loadCompanies = function() {
		$http.post(httpCompany.enlist, {})
		.success(function(data) {
			$scope.companies = data;
		});
	};






	$scope.submitAdd = function(form) {
		// check for errors first
		// set all fields to "touched"
		angular.forEach(form.$error, function(field) {
			angular.forEach(field, function(errorField) {
				errorField.$setTouched();
			});
		});


		// alert error message if errors detected
		// else submit the form
		if(!utilityFunctions.isEmptyObject(form.$error)) {
			alert('See and fix the errors to save changes!');
		} else {
			$http.post(httpJobVacancy.save, { info: $scope.jobVacancy })
			.success(function(data) {
				$state.go('admin.jobVacancyList');
			});
		}
	};



	$scope.backToJobVacancies = function() {
		var backConfirm = confirm('Discard changes and go back to job vacancies?');
		if(backConfirm) $state.go('admin.jobVacancyList');
	};



	// function for add/remove field
	$scope.addRequirementField = function() {
		$scope.jobVacancy.requirements.push('');
	};
	$scope.removeRequirementField = function() {
		$scope.jobVacancy.requirements.splice(($scope.jobVacancy.requirements.length - 1), 1);
	};





	$scope.loadCompanies();




}]);