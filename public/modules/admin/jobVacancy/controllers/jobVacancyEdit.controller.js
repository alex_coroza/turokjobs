angular.module('jobVacancyModule')
.controller('jobVacancyEditController', ['$scope', '$rootScope', '$http', '$state', '$stateParams', 'utilityFunctions', 'regexPattern', 'httpJobVacancy', 'httpCompany', function($scope, $rootScope, $http, $state, $stateParams, utilityFunctions, regexPattern, httpJobVacancy, httpCompany) {
	console.log('jobVacancyEditController initialized!');


	$scope.utilityFunctions = utilityFunctions;

	if(!utilityFunctions.inArray('Job Vacancies >> Edit', $scope.user.features)) {
		$state.go('admin.profileInfo');
		return;
	}


	$scope.$emit('activeLinkUpdate', { value: 'admin.jobVacancy' });


	$scope.alpha = regexPattern.alpha();
	$scope.alphanumeric = regexPattern.alphanumeric();
	$scope.numeric = regexPattern.numeric();
	$scope.alphanumericSymbol = regexPattern.alphanumericSymbol();

	
	$scope.jobVacancyId = $state.params.id;
	$scope.jobVacancy = {};
	$scope.currentJobVacancy = {};
	$scope.companies = [];



	$rootScope.loadingProperties.message = 'Loading job vacancy...';
	$rootScope.loadingProperties.promise = $http.post(httpJobVacancy.enlist, { id: $scope.jobVacancyId })
	.success(function(data) {
		$scope.jobVacancy = data;
		$scope.currentJobVacancy = angular.copy(data);
	});





	$scope.submitEdit = function(form) {
		// check for errors first
		// set all fields to "touched"
		angular.forEach(form.$error, function(field) {
			angular.forEach(field, function(errorField) {
				errorField.$setTouched();
			});
		});


		// alert error message if errors detected
		// else submit the form
		if(!utilityFunctions.isEmptyObject(form.$error)) {
			alert('See and fix the errors to save changes!');
		} else {
			$rootScope.loadingProperties.message = 'Saving changes...';
			$rootScope.loadingProperties.promise = $http.post(httpJobVacancy.save, { info: $scope.jobVacancy })
			.success(function(data) {
				$state.go('admin.jobVacancyInfo', { id: $scope.jobVacancyId });
			});
		}

	};




	$scope.loadCompanies = function() {
		$rootScope.loadingProperties.promise = $http.post(httpCompany.enlist, {})
		.success(function(data) {
			$scope.companies = data;
		});
	};


	
	$scope.backToJobVacancyInfo = function() {
		var discardConfirm = confirm('Discard changes and go back to job vacancy info?');
		if(discardConfirm) $state.go('admin.jobVacancyInfo', { id: $scope.jobVacancyId });
	};




	// function for add/remove field
	$scope.addRequirementField = function() {
		$scope.jobVacancy.requirements.push('');
	};
	$scope.removeRequirementField = function() {
		$scope.jobVacancy.requirements.splice(($scope.jobVacancy.requirements.length - 1), 1);
	};




	$scope.loadCompanies();




}]);