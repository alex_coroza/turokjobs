angular.module('jobVacancyModule')
.controller('jobVacancyInfoController', ['$scope', '$rootScope', '$http', '$state', '$stateParams', '$uibModal', 'utilityFunctions', 'httpJobVacancy', 'httpJobVacancyApplicationStage', 'jobVacancyApplicationServices', 'jobVacancyApplicationStageServices', function($scope, $rootScope, $http, $state, $stateParams, $uibModal, utilityFunctions, httpJobVacancy, httpJobVacancyApplicationStage, jobVacancyApplicationServices, jobVacancyApplicationStageServices) {
	console.log('jobVacancyInfoController initialized!');

	$scope.utilityFunctions = utilityFunctions;

	if(!utilityFunctions.inArray('Job Vacancies >> Info', $scope.user.features)) {
		$state.go('admin.profileInfo');
		return;
	}
	

	$scope.$emit('activeLinkUpdate', { value: 'admin.jobVacancy' });

	
	$scope.jobVacancyId = $state.params.id;
	$scope.jobVacancy = {};



	$scope.loadJobVacancy = function() {
		$rootScope.loadingProperties.message = 'Loading job vacancy...';
		$rootScope.loadingProperties.promise = $http.post(httpJobVacancy.enlist, { id: $scope.jobVacancyId, populate:['company', 'applications.user', 'applications.candidate', 'applications.stages'] })
		.success(function(data) {
			$scope.jobVacancy = data;
		});
	};





	$scope.showAddApplicants = function() {
		jobVacancyApplicationServices.showAddApplicants($scope.jobVacancy, $scope.user).then(function(response) {
			$scope.loadJobVacancy();
		});
	};


	

	$scope.showEditJobApplication = function(application) {
		jobVacancyApplicationServices.showEditJobApplication(application, $scope.user).then(function(response) {
			$scope.loadJobVacancy();
		});
	};




	$scope.showAddApplicationStage = function(jobVacancyApplication) {
		jobVacancyApplicationStageServices.showAddApplicationStage(jobVacancyApplication, $scope.user).then(function(response) {
			$scope.loadJobVacancy();
		});
	};


	$scope.applicationStageEdit = function(jobVacancyApplicationStage) {
		jobVacancyApplicationStageServices.showEditApplicationStage(jobVacancyApplicationStage, $scope.user).then(function(response) {
			$scope.loadJobVacancy();
		});
	};


	$scope.applicationStageDelete = function(applicationStage) {
		var deleteConfirm = confirm('Delete this process?');

		if(deleteConfirm) {
			$rootScope.loadingProperties.message = 'Deleting job vacancy...';
			$rootScope.loadingProperties.promise = $http.post(httpJobVacancyApplicationStage.delete, { id: applicationStage.id })
			.success(function(data) {
				$scope.loadJobVacancy();
			});
		}
	};







	$scope.showJobVacancyEdit = function() {
		$state.go('admin.jobVacancyEdit', { id: $scope.jobVacancyId });
	};



	$scope.backToJobJavacancies = function() {
		$state.go('admin.jobVacancyList');
	};



	$scope.loadJobVacancy();



}]);