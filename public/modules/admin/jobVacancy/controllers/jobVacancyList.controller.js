angular.module('jobVacancyModule')
.controller('jobVacancyListController', ['$scope', '$rootScope', '$http', '$state', '$uibModal', 'utilityFunctions', 'httpJobVacancy', 'httpCustomAuth', 'user', function($scope, $rootScope, $http, $state, $uibModal, utilityFunctions, httpJobVacancy, httpCustomAuth, user) {
	console.log('jobVacancyListController initialized!');


	$scope.utilityFunctions = utilityFunctions;

	if(!utilityFunctions.inArray('Job Vacancies', $scope.user.features)) {
		$state.go('admin.profileInfo');
		return;
	}


	$scope.$emit('activeLinkUpdate', { value: 'admin.jobVacancy' });


	$rootScope.loadingProperties = {
		promise: null,
	};


	$scope.user = user;
	$scope.jobVacancies = [];



	$scope.options = {
		search: '',
		orderBy: [
			['created_at', 'DESC']
		]
	};




	$scope.loadJobVacancies = function() {
		var processedOptions = angular.copy($scope.options);
		if(processedOptions.search == '') delete processedOptions.search;

		$rootScope.loadingProperties.message = 'Loading job vacancies...';
		$rootScope.loadingProperties.promise = $http.post(httpJobVacancy.enlist, processedOptions)
		.success(function(data) {
			$scope.jobVacancies = data;
		});
	};




	$scope.showVacancyInfo = function(vacancyId) {
		$state.go('admin.jobVacancyInfo', { id: vacancyId });
	};





	$scope.deleteJobVacancy = function(vacancyId) {
		$rootScope.loadingProperties.message = 'Deleting job vacancy...';
		$rootScope.loadingProperties.promise = $http.post(httpJobVacancy.delete, { id: vacancyId })
		.success(function(data) {
			$scope.loadJobVacancies();
		});
	};






	$scope.deleteJobVacancyConfirm = function(vacancy) {
		var deleteConfirm = confirm('Delete this job vacancy? "'+vacancy.title+'"');
		if(deleteConfirm) {
			$scope.deleteJobVacancy(vacancy.id);
		}
	};





	$scope.showAddJobVacancy = function() {
		$state.go('admin.jobVacancyAdd');
	};





	$scope.showJobVacancyEdit = function(vacancyId) {
		$state.go('admin.jobVacancyEdit', { id: vacancyId });
	};




	$scope.loadJobVacancies();








}]);