angular.module('jobVacancyModule')
.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
	$stateProvider
		.state('admin.jobVacancyList', {
			url: 'jobVacancies',
			templateUrl: '/modules/admin/jobVacancy/views/jobVacancyList.view.html',
			controller: 'jobVacancyListController'
		})
		.state('admin.jobVacancyAdd', {
			url: 'jobVacancyAdd',
			templateUrl: '/modules/admin/jobVacancy/views/jobVacancyAdd.view.html',
			controller: 'jobVacancyAddController'
		})
		.state('admin.jobVacancyInfo', {
			url: 'jobVacancyInfo/{id}',
			templateUrl: '/modules/admin/jobVacancy/views/jobVacancyInfo.view.html',
			controller: 'jobVacancyInfoController'
		})
		.state('admin.jobVacancyEdit', {
			url: 'jobVacancyEdit/{id}',
			templateUrl: '/modules/admin/jobVacancy/views/jobVacancyEdit.view.html',
			controller: 'jobVacancyEditController'
		})
	; /*$stateProvider closing*/
}]);