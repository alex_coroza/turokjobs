angular.module('jobVacancyApplicationModule')
.controller('jobVacancyApplicationAddCandidateController', ['$scope', '$rootScope', '$http', '$state', '$stateParams', '$uibModalInstance', 'utilityFunctions', 'regexPattern', 'httpCandidate', 'httpJobVacancy', 'user', 'jobVacancy', function($scope, $rootScope, $http, $state, $stateParams, $uibModalInstance, utilityFunctions, regexPattern, httpCandidate, httpJobVacancy, user, jobVacancy) {
	console.log('jobVacancyApplicationAddCandidateController initialized!');

	$scope.alpha = regexPattern.alpha();
	$scope.alphanumeric = regexPattern.alphanumeric();
	$scope.numeric = regexPattern.numeric();
	$scope.alphanumericSymbol = regexPattern.alphanumericSymbol();


	$scope.user = user;
	$scope.jobVacancy = jobVacancy;
	$scope.candidates = [];

	// set initial value for selecterdCandidates(from jobVacancy.applications)
	$scope.selectedCandidates = [];
	angular.forEach($scope.jobVacancy.applications, function(application, key) {
		$scope.selectedCandidates.push(application.candidate);
	});



	// options for loading candidates
	$scope.options = {
		search: '',
		populate: ['jobVacancyApplications.stages']
	};



	$scope.loadCandidates = function() {
		$http.post(httpCandidate.enlist, $scope.options)
		.success(function(data) {
			$scope.candidates = data;
		});
	};




	$scope.selectCandidate = function(candidate) {
		$scope.selectedCandidates.push(candidate);
	};



	$scope.deselectCandidate = function(key) {
		$scope.selectedCandidates.splice(key, 1);
	};



	$scope.checkIfSelected = function(candidate) {
		var x = 0;
		angular.forEach($scope.selectedCandidates, function(value, key) {
			if(candidate.id == value.id) x++;
		});

		if(x == 0) return true;
		else return false;
	};




	$scope.saveSelection = function() {
		var saveConfirm = confirm('Save selection?');

		if(saveConfirm) {
			var applications = [];

			// set selectedCandidates to jobVacancy.applications
			angular.forEach($scope.selectedCandidates, function(candidate, candidateKey) {
				var newApplication = { userId: $scope.user.id, candidateId: candidate.id, date: moment(), status: 'Pending', candidate: candidate };

				angular.forEach($scope.jobVacancy.applications, function(application, applicationKey) {
					if(application.dateClosed == null) delete application.dateClosed;
					if(application.candidateId == candidate.id) newApplication = application;
				});

				applications.push(newApplication);
			});

			$scope.jobVacancy.applications = applications;

			$http.post(httpJobVacancy.save, { info: $scope.jobVacancy })
			.success(function(data) {
				$uibModalInstance.close();
			});
		}
	};



	$scope.dismiss = function() {
		$uibModalInstance.dismiss();
	};









}]);