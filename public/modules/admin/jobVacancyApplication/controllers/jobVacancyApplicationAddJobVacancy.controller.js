angular.module('jobVacancyApplicationModule')
.controller('jobVacancyApplicationAddJobVacancyController', ['$scope', '$rootScope', '$http', '$state', '$stateParams', '$uibModalInstance', 'utilityFunctions', 'regexPattern', 'httpCandidate', 'httpJobVacancy', 'user', 'candidate', function($scope, $rootScope, $http, $state, $stateParams, $uibModalInstance, utilityFunctions, regexPattern, httpCandidate, httpJobVacancy, user, candidate) {
	console.log('jobVacancyApplicationAddJobVacancyController initialized!');

	$scope.alpha = regexPattern.alpha();
	$scope.alphanumeric = regexPattern.alphanumeric();
	$scope.numeric = regexPattern.numeric();
	$scope.alphanumericSymbol = regexPattern.alphanumericSymbol();


	$scope.user = user;
	$scope.candidate = candidate;
	$scope.jobVacancies = [];


	// set initial value for selectedJobVacancies(from candidate.jobVacancyApplications)
	$scope.selectedJobVacancies = [];
	angular.forEach($scope.candidate.jobVacancyApplications, function(application, key) {
		$scope.selectedJobVacancies.push(application.jobVacancy);
	});



	// options for loading jobVacancies
	$scope.options = {
		search: '',
		populate: ['applications.stages', 'company']
	};



	$scope.loadJobVacancies = function() {
		$http.post(httpJobVacancy.enlist, $scope.options)
		.success(function(data) {
			$scope.jobVacancies = data;
		});
	};




	$scope.selectJobVacancy = function(jobVacancy) {
		$scope.selectedJobVacancies.push(jobVacancy);
	};



	$scope.deselectJobVacancy = function(key) {
		$scope.selectedJobVacancies.splice(key, 1);
	};



	$scope.checkIfSelected = function(jobVacancy) {
		var x = 0;
		angular.forEach($scope.selectedJobVacancies, function(value, key) {
			if(jobVacancy.id == value.id) x++;
		});

		if(x == 0) return true;
		else return false;
	};




	$scope.saveSelection = function() {
		var saveConfirm = confirm('Save selection?');

		if(saveConfirm) {
			var applications = [];

			// set selectedJobVacancies to jobVacancy.applications
			angular.forEach($scope.selectedJobVacancies, function(jobVacancy, jobVacancyKey) {
				var newApplication = { userId: $scope.user.id, jobVacancyId: jobVacancy.id, date: moment(), status: 'Pending', jobVacancy: jobVacancy };

				angular.forEach($scope.candidate.jobVacancyApplications, function(application, applicationKey) {
					if(application.dateClosed == null) delete application.dateClosed;
					if(application.jobVacancyId == jobVacancy.id) newApplication = application;
				});

				applications.push(newApplication);
			});

			$scope.candidate.jobVacancyApplications = applications;

			$http.post(httpCandidate.save, { info: $scope.candidate })
			.success(function(data) {
				$uibModalInstance.close();
			});
		}
	};



	$scope.dismiss = function() {
		$uibModalInstance.dismiss();
	};









}]);