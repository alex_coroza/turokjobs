angular.module('jobVacancyApplicationModule')
.controller('jobVacancyApplicationEditController', ['$scope', '$rootScope', '$http', '$state', '$stateParams', '$uibModalInstance', 'utilityFunctions', 'regexPattern', 'httpJobVacancyApplication', 'httpCandidate', 'httpJobVacancy', 'httpUser', 'user', 'jobVacancyApplication', function($scope, $rootScope, $http, $state, $stateParams, $uibModalInstance, utilityFunctions, regexPattern, httpJobVacancyApplication, httpCandidate, httpJobVacancy, httpUser, user, jobVacancyApplication) {
	console.log('jobVacancyApplicationEditController initialized!');

	$scope.alpha = regexPattern.alpha();
	$scope.alphanumeric = regexPattern.alphanumeric();
	$scope.numeric = regexPattern.numeric();
	$scope.alphanumericSymbol = regexPattern.alphanumericSymbol();


	$scope.user = user;

	$scope.jobVacancyApplication = jobVacancyApplication;
	$scope.jobVacancies = [];
	$scope.candidates = [];
	$scope.recruiters = [];


	

	$scope.loadJobVacancies = function() {
		$http.post(httpJobVacancy.enlist, { populate: ['company'] })
		.success(function(data) {
			$scope.jobVacancies = data;
		});
	};



	$scope.loadCandidates = function() {
		$http.post(httpCandidate.enlist, {})
		.success(function(data) {
			$scope.candidates = data;
		});
	};



	$scope.loadRecruiters = function() {
		$http.post(httpUser.enlist, {})
		.success(function(data) {
			$scope.recruiters = data;
		});
	};






	$scope.saveChanges = function(form) {
		var saveConfirm = confirm('Are you sure to save changes?');

		if(saveConfirm) {
			// check for errors first
			// set all fields to "touched"
			angular.forEach(form.$error, function(field) {
				angular.forEach(field, function(errorField) {
					errorField.$setTouched();
				});
			});


			// alert error message if errors detected
			// else submit the form
			if(!utilityFunctions.isEmptyObject(form.$error)) {
				alert('See and fix the errors to save changes!');
			} else {
				$http.post(httpJobVacancyApplication.save, { info: $scope.jobVacancyApplication })
				.success(function(data) {
					$uibModalInstance.close();
				});
			}
		}
	};






	$scope.dismiss = function() {
		$uibModalInstance.dismiss();
	};






	$scope.loadJobVacancies();
	$scope.loadCandidates();
	$scope.loadRecruiters();









}]);