angular.module('jobVacancyApplicationModule')
.factory('jobVacancyApplicationServices', ['$q', '$state', '$uibModal', function($q, $state, $uibModal) {
	

	// call this from candidateInfo
	this.showAddJobApplication = function(candidate, user) {
		var deferred = $q.defer();

		$uibModal.open({
			size: 'lg',
			templateUrl: '/modules/admin/jobVacancyApplication/views/jobVacancyApplicationAddJobVacancy.tpl.html',
			controller: 'jobVacancyApplicationAddJobVacancyController',
			resolve: {
				user: function() {
					return user;
				},
				candidate: function() {
					return candidate;
				}
			}
		}).result.then(function(response) {
			deferred.resolve(response);
		});

		return deferred.promise;
	};






	// call this from jobVacancyInfo
	this.showAddApplicants = function(jobVacancy, user) {
		var deferred = $q.defer();

		$uibModal.open({
			size: 'lg',
			templateUrl: '/modules/admin/jobVacancyApplication/views/jobVacancyApplicationAddCandidate.tpl.html',
			controller: 'jobVacancyApplicationAddCandidateController',
			resolve: {
				user: function() {
					return user;
				},
				jobVacancy: function() {
					return jobVacancy;
				}
			}
		}).result.then(function(response) {
			deferred.resolve(response);
		});

		return deferred.promise;
	};






	this.showEditJobApplication = function(jobVacancyApplication, user) {
		var deferred = $q.defer();

		$uibModal.open({
			templateUrl: '/modules/admin/jobVacancyApplication/views/jobVacancyApplicationEdit.tpl.html',
			controller: 'jobVacancyApplicationEditController',
			resolve: {
				user: function() {
					return user;
				},
				jobVacancyApplication: function() {
					return jobVacancyApplication;
				}
			}
		}).result.then(function(response) {
			deferred.resolve(response);
		});

		return deferred.promise;
	};









	return this;

}]);





