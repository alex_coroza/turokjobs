angular.module('jobVacancyApplicationStageModule')
.controller('jobVacancyApplicationStageAddController', ['$scope', '$rootScope', '$http', '$state', '$stateParams', '$uibModalInstance', 'utilityFunctions', 'regexPattern', 'httpJobVacancyApplicationStage', 'user', 'jobVacancyApplication', function($scope, $rootScope, $http, $state, $stateParams, $uibModalInstance, utilityFunctions, regexPattern, httpJobVacancyApplicationStage, user, jobVacancyApplication) {
	console.log('jobVacancyApplicationStageAddController initialized!');

	$scope.alpha = regexPattern.alpha();
	$scope.alphanumeric = regexPattern.alphanumeric();
	$scope.numeric = regexPattern.numeric();
	$scope.alphanumericSymbol = regexPattern.alphanumericSymbol();


	$scope.user = user;
	$scope.jobVacancyApplication = jobVacancyApplication;
	$scope.applicationStage = {
		jobVacancyApplicationId: $scope.jobVacancyApplication.id,
		result: 'Pending'
	};

	$scope.datePickerIsOpen = false;




	$scope.submitAdd = function(form) {
		// check for errors first
		// set all fields to "touched"
		angular.forEach(form.$error, function(field) {
			angular.forEach(field, function(errorField) {
				errorField.$setTouched();
			});
		});


		// alert error message if errors detected
		// else submit the form
		if(!utilityFunctions.isEmptyObject(form.$error)) {
			alert('See and fix the errors to save changes!');
		} else {
			$http.post(httpJobVacancyApplicationStage.save, { info: $scope.applicationStage })
			.success(function(data) {
				$uibModalInstance.close();
			});
		}

	};



	$scope.dismiss = function() {
		var dismissConfirm = confirm('Cancel and discard changes?');
		if(dismissConfirm) $uibModalInstance.dismiss();
	};









}]);