angular.module('jobVacancyApplicationStageModule')
.factory('jobVacancyApplicationStageServices', ['$q', '$state', '$uibModal', function($q, $state, $uibModal) {
	



	this.showAddApplicationStage = function(jobVacancyApplication, user) {
		var deferred = $q.defer();

		$uibModal.open({
			templateUrl: '/modules/admin/jobVacancyApplicationStage/views/jobVacancyApplicationStageAdd.tpl.html',
			controller: 'jobVacancyApplicationStageAddController',
			resolve: {
				user: function() {
					return user;
				},
				jobVacancyApplication: function() {
					return jobVacancyApplication;
				}
			}
		}).result.then(function(response) {
			deferred.resolve(response);
		});

		return deferred.promise;
	};







	this.showEditApplicationStage = function(jobVacancyApplicationStage, user) {
		var deferred = $q.defer();

		$uibModal.open({
			templateUrl: '/modules/admin/jobVacancyApplicationStage/views/jobVacancyApplicationStageEdit.tpl.html',
			controller: 'jobVacancyApplicationStageEditController',
			resolve: {
				user: function() {
					return user;
				},
				jobVacancyApplicationStage: function() {
					return jobVacancyApplicationStage;
				}
			}
		}).result.then(function(response) {
			deferred.resolve(response);
		});

		return deferred.promise;
	};

















	return this;

}]);





