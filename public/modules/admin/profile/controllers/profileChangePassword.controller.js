angular.module('profileModule')
.controller('profileChangePasswordController', ['$q', '$scope', '$rootScope', '$http', '$state', '$stateParams', 'utilityFunctions', 'formUtilities', 'regexPattern', 'httpUser', 'httpCustomAuth', 'user', function($q, $scope, $rootScope, $http, $state, $stateParams, utilityFunctions, formUtilities, regexPattern, httpUser, httpCustomAuth, user) {
	console.log('profileChangePasswordController initialized!');

	$scope.$emit('activeLinkUpdate', { value: 'admin.profile' });

	$scope.alpha = regexPattern.alpha();
	$scope.alphanumeric = regexPattern.alphanumeric();
	$scope.numeric = regexPattern.numeric();
	$scope.alphanumericSymbol = regexPattern.alphanumericSymbol();

	
	$scope.profile = user;



	$scope.submitChangePassword = function(form) {
		// check for errors first
		// set all fields to "touched"
		angular.forEach(form.$error, function(field) {
			angular.forEach(field, function(errorField) {
				errorField.$setTouched();
			});
		});

		// alert error message if errors detected
		// else submit the form
		if(!utilityFunctions.isEmptyObject(form.$error)) {
			alert('See and fix the errors to save changes!');
		} else {
			$rootScope.loadingProperties.message = 'Checking password...';
			$rootScope.loadingProperties.promise = $http.post(httpCustomAuth.accessCheckPassword, { email: $scope.profile.email, password: $scope.profile.password })
			.then(function(response) {
				if(response.data > 0) {
					$rootScope.loadingProperties.message = 'Saving changes...';
					return $rootScope.loadingProperties.promise = $http.post(httpUser.save, { info: $scope.profile })
				} else {
					return $q.reject('Wrong password!');
				}
			}).then(function(response) {
				alert('Change password successful!');
				$state.go('admin.profileInfo');
			}).catch(function(errorMessage) {
				alert(errorMessage);
			});
		}
	};







	
	$scope.backToProfile = function() {
		var discardConfirm = confirm('Cancel and discard changes?');
		if(discardConfirm) $state.go('admin.profileInfo');
	};









}]);