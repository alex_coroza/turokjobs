angular.module('recruiterModule')
.controller('profileInfoController', ['$scope', '$rootScope', '$http', '$state', '$stateParams', 'utilityFunctions', 'regexPattern', 'httpUser', 'user', function($scope, $rootScope, $http, $state, $stateParams, utilityFunctions, regexPattern, httpUser, user) {
	console.log('profileInfoController initialized!');

	$scope.$emit('activeLinkUpdate', { value: 'admin.profile' });

	$scope.alpha = regexPattern.alpha();
	$scope.alphanumeric = regexPattern.alphanumeric();
	$scope.numeric = regexPattern.numeric();
	$scope.alphanumericSymbol = regexPattern.alphanumericSymbol();

	
	$scope.user = user;
	$scope.profile = {};



	$rootScope.loadingProperties.message = 'Loading profile...';
	$rootScope.loadingProperties.promise = $http.post(httpUser.enlist, { id: $scope.user.id, populate: ['applications.candidate', 'applications.jobVacancy.company'] })
	.success(function(data) {
		$scope.profile = data;
	});





	
	$scope.changePassword = function() {
		$state.go('admin.profileChangePassword');
	};










}]);