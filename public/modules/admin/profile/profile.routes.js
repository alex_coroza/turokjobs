angular.module('profileModule')
.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
	$stateProvider
		.state('admin.profileChangePassword', {
			url: 'profileChangePassword',
			templateUrl: '/modules/admin/profile/views/profileChangePassword.view.html',
			controller: 'profileChangePasswordController'
		})
		.state('admin.profileInfo', {
			url: 'profileInfo',
			templateUrl: '/modules/admin/profile/views/profileInfo.view.html',
			controller: 'profileInfoController'
		})
	; /*$stateProvider closing*/
}]);