angular.module('recruiterModule')
.controller('recruiterAddController', ['$scope', '$rootScope', '$http', '$state', '$stateParams', 'utilityFunctions', 'regexPattern', 'httpUser', 'user', function($scope, $rootScope, $http, $state, $stateParams, utilityFunctions, regexPattern, httpUser, user) {
	console.log('recruiterAddController initialized!');

	if(!utilityFunctions.inArray('Recruiters >> Add', $scope.user.features)) {
		$state.go('admin.profileInfo');
		return;
	}


	$scope.$emit('activeLinkUpdate', { value: 'admin.recruiter' });

	$scope.alpha = regexPattern.alpha();
	$scope.alphanumeric = regexPattern.alphanumeric();
	$scope.numeric = regexPattern.numeric();
	$scope.alphanumericSymbol = regexPattern.alphanumericSymbol();


	$scope.user = user;
	$scope.recruiter = {
		type: 'Recruiter'
	};	
	




	// $scope.checkEmail = function() {
	// 	$http.post(httpUser.checkEmail, { info: $scope.recruiter.email })
	// 	.success(function(data) {

	// 	});
	// };




	





	$scope.submitAdd = function(form) {
		// check for errors first
		// set all fields to "touched"
		angular.forEach(form.$error, function(field) {
			angular.forEach(field, function(errorField) {
				errorField.$setTouched();
			});
		});

		// alert error message if errors detected
		// else submit the form
		if(!utilityFunctions.isEmptyObject(form.$error)) {
			alert('See and fix the errors to save changes!');
		} else {
			$rootScope.loadingProperties.message = 'Adding recruiter...';
			$rootScope.loadingProperties.promise = $http.post(httpUser.register, { info: $scope.recruiter })
			.success(function(data) {
				if(data == 'error') {
					alert('Email already in use!');
				} else {
					alert('Account created! Password is '+data+'. Please change it immediately on recruiter\'s account!');
					$state.go('admin.recruiterList');
				}
			});
		}

	};



	$scope.backToRecruiterList = function() {
		var backConfirm = confirm('Discard changes and go back to recruiter list?');
		if(backConfirm) $state.go('admin.recruiterList');
	};










}]);