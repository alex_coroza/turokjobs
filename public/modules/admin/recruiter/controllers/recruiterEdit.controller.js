angular.module('recruiterModule')
.controller('recruiterEditController', ['$scope', '$rootScope', '$http', '$state', '$stateParams', 'utilityFunctions', 'regexPattern', 'httpUser', function($scope, $rootScope, $http, $state, $stateParams, utilityFunctions, regexPattern, httpUser) {
	console.log('recruiterEditController initialized!');

	if(!utilityFunctions.inArray('Recruiters >> Edit', $scope.user.features)) {
		$state.go('admin.profileInfo');
		return;
	}


	$scope.$emit('activeLinkUpdate', { value: 'admin.recruiter' });

	$scope.alpha = regexPattern.alpha();
	$scope.alphanumeric = regexPattern.alphanumeric();
	$scope.numeric = regexPattern.numeric();
	$scope.alphanumericSymbol = regexPattern.alphanumericSymbol();

	
	$scope.recruiterId = $state.params.id;
	$scope.recruiter = {};
	$scope.currentRecruiter = {};



	$rootScope.loadingProperties.message = 'Loading recruiter...';
		$rootScope.loadingProperties.promise = $http.post(httpUser.enlist, { id: $scope.recruiterId })
	.success(function(data) {
		$scope.recruiter = data;
		$scope.currentRecruiter = angular.copy(data);
	});




	$scope.submitEdit = function(form) {
		// check for errors first
		// set all fields to "touched"
		angular.forEach(form.$error, function(field) {
			angular.forEach(field, function(errorField) {
				errorField.$setTouched();
			});
		});

		// alert error message if errors detected
		// else submit the form
		if(!utilityFunctions.isEmptyObject(form.$error)) {
			alert('See and fix the errors to save changes!');
		} else {
			$rootScope.loadingProperties.message = 'Saving changes...';
			$rootScope.loadingProperties.promise = $http.post(httpUser.save, { info: $scope.recruiter })
			.success(function(data) {
				if(data == 'error' && $scope.recruiter.email != $scope.currentRecruiter.email) {
					alert('Email already in use!');
				} else {
					$state.go('admin.recruiterInfo', { id: $scope.recruiterId });
				}
			});
		}
	};


	
	$scope.backToRecruiterInfo = function() {
		var discardConfirm = confirm('Cancel and discard changes?');
		if(discardConfirm) $state.go('admin.recruiterInfo', { id: $scope.recruiterId });
	};









}]);