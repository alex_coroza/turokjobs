angular.module('recruiterModule')
.controller('recruiterInfoController', ['$scope', '$rootScope', '$http', '$state', '$stateParams', 'utilityFunctions', 'regexPattern', 'httpUser', 'accessManagementServices', function($scope, $rootScope, $http, $state, $stateParams, utilityFunctions, regexPattern, httpUser, accessManagementServices) {
	console.log('recruiterInfoController initialized!');

	$scope.utilityFunctions = utilityFunctions;

	if(!utilityFunctions.inArray('Recruiters >> Info', $scope.user.features)) {
		$state.go('admin.profileInfo');
		return;
	}


	$scope.$emit('activeLinkUpdate', { value: 'admin.recruiter' });

	$scope.alpha = regexPattern.alpha();
	$scope.alphanumeric = regexPattern.alphanumeric();
	$scope.numeric = regexPattern.numeric();
	$scope.alphanumericSymbol = regexPattern.alphanumericSymbol();

	
	$scope.recruiterId = $state.params.id;
	$scope.recruiter = {};



	$scope.loadRecruiter = function() {
		$rootScope.loadingProperties.message = 'Loading recruiter...';
		$rootScope.loadingProperties.promise = $http.post(httpUser.enlist, { id: $scope.recruiterId, populate: ['applications.candidate', 'applications.jobVacancy.company'] })
		.success(function(data) {
			$scope.recruiter = data;
		});
	};



	$scope.resetPassword = function() {
		var resetConfirm = confirm('Reset password for this recruiter?');
		if(resetConfirm) {
			$rootScope.loadingProperties.message = 'Reseting password...';
			$rootScope.loadingProperties.promise = $http.post(httpUser.resetPassword, { userId: $scope.recruiterId })
			.success(function(data) {
				alert($scope.recruiter.name+'\'s new password is '+data);
			});
		}
	};

	
	$scope.backToRecruiterList = function() {
		$state.go('admin.recruiterList');
	};

	$scope.showRecruiterEdit = function() {
		$state.go('admin.recruiterEdit', { id: $scope.recruiterId });
	};





	$scope.showAccessManagement = function() {
		accessManagementServices.showAccessManagement($scope.recruiter, $scope.user).then(function(response) {
			$scope.loadRecruiter();
		});
	};





	$scope.loadRecruiter();



}]);