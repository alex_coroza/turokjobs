angular.module('recruiterModule')
.controller('recruiterListController', ['$scope', '$rootScope', '$http', '$state', '$stateParams', 'utilityFunctions', 'httpUser', function($scope, $rootScope, $http, $state, $stateParams, utilityFunctions, httpUser) {
	console.log('recruiterListController initialized!');

	$scope.utilityFunctions = utilityFunctions;

	if(!utilityFunctions.inArray('Recruiters', $scope.user.features)) {
		$state.go('admin.profileInfo');
		return;
	}


	$scope.$emit('activeLinkUpdate', { value: 'admin.recruiter' });
	

	$scope.recruiters = [];
	$scope.options = {
		search: '',
		populate: ['applications']
	};


	

	$scope.loadRecruiters = function() {
		$rootScope.loadingProperties.message = 'Loading recruiters...';
		$rootScope.loadingProperties.promise = $http.post(httpUser.enlist, $scope.options)
		.success(function(data) {
			$scope.recruiters = data;
		});
	};



	$scope.showRecruiterInfo = function(recruiterId) {
		$state.go('admin.recruiterInfo', { id: recruiterId });
	};



	$scope.showAddRecruiter = function() {
		$state.go('admin.recruiterAdd');
	};



	$scope.showRecruiterEdit = function(recruiterId) {
		$state.go('admin.recruiterEdit', { id: recruiterId });
	};







	$scope.loadRecruiters();


}]);