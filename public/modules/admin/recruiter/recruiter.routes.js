angular.module('recruiterModule')
.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
	$urlRouterProvider.when('', '/dashboard/');
	$stateProvider
		.state('admin.recruiterList', {
			url: 'recruiterList/',
			templateUrl: '/modules/admin/recruiter/views/recruiterList.view.html',
			controller: 'recruiterListController'
		})
		.state('admin.recruiterAdd', {
			url: 'recruiterAdd/',
			templateUrl: '/modules/admin/recruiter/views/recruiterAdd.view.html',
			controller: 'recruiterAddController'
		})
		.state('admin.recruiterEdit', {
			url: 'recruiterEdit/{id}',
			templateUrl: '/modules/admin/recruiter/views/recruiterEdit.view.html',
			controller: 'recruiterEditController'
		})
		.state('admin.recruiterInfo', {
			url: 'recruiterInfo/{id}',
			templateUrl: '/modules/admin/recruiter/views/recruiterInfo.view.html',
			controller: 'recruiterInfoController'
		})
	; /*$stateProvider closing*/
}]);