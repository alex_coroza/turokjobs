angular.module('angularUiMods.directive', []);



angular.module('angularUiMods.directive')


.directive('stringDate', ['$timeout', function ($timeout){
	return {
		restrict: 'A',
		require: 'ngModel',
		link: function(scope, elem, attrs, ngModel) {
			ngModel.$formatters.push(function(input) {
				if(input == null) return '';
				else return new Date(input);
			});
		}
	}
}]);





; // closing semi-colon