angular.module('utility.directive', []);



// sets of utility functions
angular.module('utility.directive')


.directive('triggerClick', [function() {
	
	return {
		restrict: 'A',
		link: function(scope, element, attrs) {
			element.bind('click', function(e) {
				angular.element(attrs.triggerClick).trigger('click');
			});
		}
	};

}])





; // closing semi-colon