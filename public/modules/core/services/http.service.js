var DOMAIN = 'http://localhost:8000/';

angular.module('http.service', [])


.factory('httpJobVacancy', [function() {
	this.enlist = DOMAIN+'jobVacancy/enlist';
	this.save = DOMAIN+'jobVacancy/save';
	this.delete = DOMAIN+'jobVacancy/delete';

	return this;
}])


.factory('httpJobVacancyApplication', [function() {
	this.enlist = DOMAIN+'jobVacancyApplication/enlist';
	this.save = DOMAIN+'jobVacancyApplication/save';
	this.delete = DOMAIN+'jobVacancyApplication/delete';

	return this;
}])


.factory('httpJobVacancyApplicationStage', [function() {
	// this.enlist = DOMAIN+'jobVacancyApplicationStage/enlist';
	this.save = DOMAIN+'jobVacancyApplicationStage/save';
	this.delete = DOMAIN+'jobVacancyApplicationStage/delete';

	return this;
}])


.factory('httpCompany', [function() {
	this.enlist = DOMAIN+'company/enlist';
	this.save = DOMAIN+'company/save';
	this.delete = DOMAIN+'company/delete';

	return this;
}])


.factory('httpCandidate', [function() {
	this.enlist = DOMAIN+'candidate/enlist';
	this.save = DOMAIN+'candidate/save';
	this.delete = DOMAIN+'candidate/delete';
	this.uploadFile = DOMAIN+'candidate/uploadFile';
	this.deleteFile = DOMAIN+'candidate/deleteFile';

	return this;
}])


.factory('httpUser', [function() {
	this.enlist = DOMAIN+'user/enlist';
	this.register = DOMAIN+'user/register';
	this.save = DOMAIN+'user/save';
	this.resetPassword = DOMAIN+'user/resetPassword';
	this.accessCheckEmail = DOMAIN+'user/accessCheckEmail';

	return this;
}])


.factory('httpMainDashboard', [function() {
	this.kpiCount = DOMAIN+'mainDashboard/kpiCount';

	return this;
}])


.factory('httpFeature', [function() {
	this.enlist = DOMAIN+'feature/enlist';

	return this;
}])




.factory('httpCustomAuth', [function() {
	this.loginInfo = DOMAIN+'auth/loginInfo';
	this.login = DOMAIN+'auth/login';
	this.logout = DOMAIN+'auth/logout';
	this.accessCheckPassword = DOMAIN+'auth/accessCheckPassword';

	return this;
}])






; /*closing semi-colon*/
