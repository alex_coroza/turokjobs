angular.module('guestModule', [
	'ngMessages', 'ngAnimate', 'ui.router', 'ui.bootstrap',
	'angularMoment', 'cgBusy',
	'utility.service', 'http.service',
	'homeModule', 'loginModule'
]);



// angular-busy override settings
angular.module('guestModule')
.value('cgBusyDefaults', {
	message:'Loading...',
	backdrop: true,
	// templateUrl: 'my_custom_template.html',
	delay: 0,
	minDuration: 1000,
	// wrapperClass: 'my-class my-class2'
});
