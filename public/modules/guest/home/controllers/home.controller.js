angular.module('homeModule')
.controller('homeController', ['$scope', '$rootScope', '$http', '$state', '$uibModal', 'utilityFunctions', 'httpJobVacancy', function($scope, $rootScope, $http, $state, $uibModal, utilityFunctions, httpJobVacancy) {
	console.log('homeController initialized!');


	$rootScope.loadingProperties = {
		promise: null,
	};


	$scope.jobVacancies = [];



	$scope.options = {
		search: '',
		orderBy: [
			['created_at', 'DESC']
		]
	};





	$scope.loadJobVacancies = function() {
		var processedOptions = angular.copy($scope.options);
		if(processedOptions.search == '') delete processedOptions.search;

		$rootScope.loadingProperties.message = 'Loading job vacancies...';
		$rootScope.loadingProperties.promise = $http.post(httpJobVacancy.enlist, processedOptions)
		.success(function(data) {
			$scope.jobVacancies = data;
		});
	};




	$scope.showVacancyInfo = function(vacancyId) {
		$state.go('guest.jobVacancyInfo', { id: vacancyId });
	};





	$scope.home = function() {
		window.location = '/';
	};







	$scope.loadJobVacancies();


	



}]);