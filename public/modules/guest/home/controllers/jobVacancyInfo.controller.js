angular.module('homeModule')
.controller('jobVacancyInfoController', ['$scope', '$rootScope', '$http', '$state', '$stateParams', 'utilityFunctions', 'httpJobVacancy', function($scope, $rootScope, $http, $state, $stateParams, utilityFunctions, httpJobVacancy) {
	console.log('jobVacancyInfoController initialized!');

	$rootScope.loadingProperties = {
		promise: null,
	};
	
	
	$scope.jobVacancyId = $stateParams.id;
	$scope.jobVacancy = {};



	$rootScope.loadingProperties.message = 'Loading job vacancy info...';
	$rootScope.loadingProperties.promise = $http.post(httpJobVacancy.enlist, { id: $scope.jobVacancyId })
	.success(function(data) {
		$scope.jobVacancy = data;
	});


	
	$scope.home = function() {
		$state.go('guest.home');
	};


}]);