angular.module('homeModule')
.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
	$urlRouterProvider.when('', '/home');
	$stateProvider
		.state('guest.home', {
			url: 'home',
			templateUrl: '/modules/guest/home/views/home.view.html',
			controller: 'homeController'
		})
		.state('guest.jobVacancyInfo', {
			url: 'job-vacancy-info/{id}',
			templateUrl: '/modules/guest/home/views/jobVacancyInfo.view.html',
			controller: 'jobVacancyInfoController'
		})
	; /*$stateProvider closing*/
}]);