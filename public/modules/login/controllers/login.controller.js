angular.module('loginModule')
.controller('loginController', ['$scope', '$rootScope', '$http', '$state', '$uibModal', 'utilityFunctions', 'regexPattern', 'httpCustomAuth', function($scope, $rootScope, $http, $state, $uibModal, utilityFunctions, regexPattern, httpCustomAuth) {
	console.log('loginController initialized!');

	$rootScope.loadingProperties = {
		promise: null,
	};

	$scope.alpha = regexPattern.alpha();
	$scope.alphanumeric = regexPattern.alphanumeric();
	$scope.numeric = regexPattern.numeric();
	$scope.alphanumericSymbol = regexPattern.alphanumericSymbol();


	$scope.email = '';
	$scope.password = '';


	$scope.login = function(form) {
		// check for errors first
		// set all fields to "touched"
		angular.forEach(form.$error, function(field) {
			angular.forEach(field, function(errorField) {
				errorField.$setTouched();
			});
		});


		// alert error message if errors detected
		// else submit the form
		if(!utilityFunctions.isEmptyObject(form.$error)) {
			alert('See and fix the errors before proceeding!');
		} else {
			$rootScope.loadingProperties.message = 'Logging in...';
			$rootScope.loadingProperties.promise = $http.post(httpCustomAuth.login, { email: $scope.email, password: $scope.password })
			.success(function(data) {
				if(data == 'error') {
					alert('Invalid email or password!');
				} else {
					localStorage.setItem("turokJobsUserSession", JSON.stringify(data));
					window.location = '/admin/';
				}
			});
		}
	};





	$scope.home = function() {
		$state.go('guest.home');
	};



}]);