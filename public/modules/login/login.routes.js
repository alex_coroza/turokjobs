angular.module('loginModule')
.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
	$stateProvider
		.state('guest.login', {
			url: 'login',
			templateUrl: '/modules/login/views/login.view.html',
			controller: 'loginController'
		})
	; /*closing $stateProvider*/
}]);

				