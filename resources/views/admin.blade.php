 <!DOCTYPE html>
<html lang="en" ng-app="adminModule">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<title>Kirby's Turok Jobs</title>

		<link rel="stylesheet" type="text/css" href="/assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="/assets/bower_components/angular-bootstrap/ui-bootstrap-csp.css">
		<link rel="stylesheet" type="text/css" href="/assets/bower_components/angular-motion/dist/angular-motion.min.css">
		<link rel="stylesheet" type="text/css" href="/assets/bower_components/mdi/css/materialdesignicons.min.css">
		<link rel="stylesheet" type="text/css" href="/assets/bower_components/angular-busy/dist/angular-busy.min.css">
		<link rel="stylesheet" type="text/css" href="/assets/css/admin.css">
	</head>
	<body>
		




		<div ui-view class="am-fade"></div>







		<!-- JS INCLUDES -->
		<!-- core js -->
		<script type="text/javascript" src="/assets/bower_components/angular/angular.js"></script>
		<script type="text/javascript" src="/assets/bower_components/angular-animate/angular-animate.min.js"></script>
		<script type="text/javascript" src="/assets/bower_components/angular-messages/angular-messages.min.js"></script>
		<script type="text/javascript" src="/assets/bower_components/angular-ui-router/release/angular-ui-router.min.js"></script>
		<script type="text/javascript" src="/assets/bower_components/angular-bootstrap/ui-bootstrap.min.js"></script>
		<script type="text/javascript" src="/assets/bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js"></script>

		<!-- third party plugins -->
		<script type="text/javascript" src="/assets/bower_components/moment/moment.js"></script>
		<script type="text/javascript" src="/assets/bower_components/angular-moment/angular-moment.min.js"></script>
		<script type="text/javascript" src="/assets/bower_components/ng-file-upload/ng-file-upload.min.js"></script>
		<script type="text/javascript" src="/assets/bower_components/angular-busy/dist/angular-busy.min.js"></script>

		<!-- core utility files -->
		<script type="text/javascript" src="/modules/core/services/utility.service.js"></script>
		<script type="text/javascript" src="/modules/core/services/http.service.js"></script>
		<script type="text/javascript" src="/modules/core/directives/angularUiMods.directive.js"></script>

		<!-- dashboardModule files -->
		<script type="text/javascript" src="/modules/admin/dashboard/dashboard.module.js"></script>
		<script type="text/javascript" src="/modules/admin/dashboard/dashboard.routes.js"></script>
		<script type="text/javascript" src="/modules/admin/dashboard/controllers/dashboard.controller.js"></script>

		<!-- jobVacancyModule files -->
		<script type="text/javascript" src="/modules/admin/jobVacancy/jobVacancy.module.js"></script>
		<script type="text/javascript" src="/modules/admin/jobVacancy/jobVacancy.routes.js"></script>
		<script type="text/javascript" src="/modules/admin/jobVacancy/controllers/jobVacancyList.controller.js"></script>
		<script type="text/javascript" src="/modules/admin/jobVacancy/controllers/jobVacancyInfo.controller.js"></script>
		<script type="text/javascript" src="/modules/admin/jobVacancy/controllers/jobVacancyEdit.controller.js"></script>
		<script type="text/javascript" src="/modules/admin/jobVacancy/controllers/jobVacancyAdd.controller.js"></script>

		<!-- jobVacancyApplicationModule files -->
		<script type="text/javascript" src="/modules/admin/jobVacancyApplication/jobVacancyApplication.module.js"></script>
		<script type="text/javascript" src="/modules/admin/jobVacancyApplication/services/jobVacancyApplication.service.js"></script>
		<script type="text/javascript" src="/modules/admin/jobVacancyApplication/controllers/jobVacancyApplicationAddCandidate.controller.js"></script>
		<script type="text/javascript" src="/modules/admin/jobVacancyApplication/controllers/jobVacancyApplicationAddJobVacancy.controller.js"></script>
		<script type="text/javascript" src="/modules/admin/jobVacancyApplication/controllers/jobVacancyApplicationEdit.controller.js"></script>

		<!-- jobVacancyApplicationStageModule files -->
		<script type="text/javascript" src="/modules/admin/jobVacancyApplicationStage/jobVacancyApplicationStage.module.js"></script>
		<script type="text/javascript" src="/modules/admin/jobVacancyApplicationStage/services/jobVacancyApplicationStage.service.js"></script>
		<script type="text/javascript" src="/modules/admin/jobVacancyApplicationStage/controllers/jobVacancyApplicationStageAdd.controller.js"></script>
		<script type="text/javascript" src="/modules/admin/jobVacancyApplicationStage/controllers/jobVacancyApplicationStageEdit.controller.js"></script>

		<!-- candidateModule files -->
		<script type="text/javascript" src="/modules/admin/candidate/candidate.module.js"></script>
		<script type="text/javascript" src="/modules/admin/candidate/candidate.routes.js"></script>
		<script type="text/javascript" src="/modules/admin/candidate/controllers/candidateList.controller.js"></script>
		<script type="text/javascript" src="/modules/admin/candidate/controllers/candidateInfo.controller.js"></script>
		<script type="text/javascript" src="/modules/admin/candidate/controllers/candidateAdd.controller.js"></script>
		<script type="text/javascript" src="/modules/admin/candidate/controllers/candidateEdit.controller.js"></script>

		<!-- companyModule files -->
		<script type="text/javascript" src="/modules/admin/company/company.module.js"></script>
		<script type="text/javascript" src="/modules/admin/company/company.routes.js"></script>
		<script type="text/javascript" src="/modules/admin/company/controllers/companyList.controller.js"></script>
		<script type="text/javascript" src="/modules/admin/company/controllers/companyAdd.controller.js"></script>
		<script type="text/javascript" src="/modules/admin/company/controllers/companyEdit.controller.js"></script>
		<script type="text/javascript" src="/modules/admin/company/controllers/companyInfo.controller.js"></script>

		<!-- recruiterModule files -->
		<script type="text/javascript" src="/modules/admin/recruiter/recruiter.module.js"></script>
		<script type="text/javascript" src="/modules/admin/recruiter/recruiter.routes.js"></script>
		<script type="text/javascript" src="/modules/admin/recruiter/controllers/recruiterList.controller.js"></script>
		<script type="text/javascript" src="/modules/admin/recruiter/controllers/recruiterAdd.controller.js"></script>
		<script type="text/javascript" src="/modules/admin/recruiter/controllers/recruiterEdit.controller.js"></script>
		<script type="text/javascript" src="/modules/admin/recruiter/controllers/recruiterInfo.controller.js"></script>

		<!-- profileModule files -->
		<script type="text/javascript" src="/modules/admin/profile/profile.module.js"></script>
		<script type="text/javascript" src="/modules/admin/profile/profile.routes.js"></script>
		<script type="text/javascript" src="/modules/admin/profile/controllers/profileInfo.controller.js"></script>
		<script type="text/javascript" src="/modules/admin/profile/controllers/profileChangePassword.controller.js"></script>

		<!-- accessmanagementModule files -->
		<script type="text/javascript" src="/modules/admin/accessManagement/accessManagement.module.js"></script>
		<script type="text/javascript" src="/modules/admin/accessManagement/services/accessManagement.service.js"></script>
		<script type="text/javascript" src="/modules/admin/accessManagement/controllers/accessManagement.controller.js"></script>

		<!-- admin app base module files -->
		<script type="text/javascript" src="/modules/admin/admin.module.js"></script>
		<script type="text/javascript" src="/modules/admin/admin.routes.js"></script>
		<script type="text/javascript" src="/modules/admin/admin.controller.js"></script>

	</body>
</html>
