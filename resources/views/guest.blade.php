<!DOCTYPE html>
<html lang="en" ng-app="guestModule">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<title>Kirby's Turok Jobs</title>

		<link rel="stylesheet" type="text/css" href="/assets/bower_components/bootstrap/dist/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="/assets/bower_components/angular-bootstrap/ui-bootstrap-csp.css">
		<link rel="stylesheet" type="text/css" href="/assets/bower_components/angular-motion/dist/angular-motion.min.css">
		<link rel="stylesheet" type="text/css" href="/assets/bower_components/mdi/css/materialdesignicons.min.css">
		<link rel="stylesheet" type="text/css" href="/assets/bower_components/angular-busy/dist/angular-busy.min.css">
		<link rel="stylesheet" type="text/css" href="/assets/css/guest.css">
	</head>
	<body>
		




		<div ui-view class="am-fade"></div>







		<!-- JS INCLUDES -->
		<!-- core js -->
		<script type="text/javascript" src="/assets/bower_components/angular/angular.js"></script>
		<script type="text/javascript" src="/assets/bower_components/angular-animate/angular-animate.min.js"></script>
		<script type="text/javascript" src="/assets/bower_components/angular-messages/angular-messages.min.js"></script>
		<script type="text/javascript" src="/assets/bower_components/angular-ui-router/release/angular-ui-router.min.js"></script>
		<script type="text/javascript" src="/assets/bower_components/angular-bootstrap/ui-bootstrap.min.js"></script>
		<script type="text/javascript" src="/assets/bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js"></script>

		<!-- third party plugins -->
		<script type="text/javascript" src="/assets/bower_components/moment/moment.js"></script>
		<script type="text/javascript" src="/assets/bower_components/angular-moment/angular-moment.min.js"></script>
		<script type="text/javascript" src="/assets/bower_components/angular-busy/dist/angular-busy.min.js"></script>

		<!-- core utility files -->
		<script type="text/javascript" src="/modules/core/services/utility.service.js"></script>
		<script type="text/javascript" src="/modules/core/services/http.service.js"></script>

		<!-- homeModule files -->
		<script type="text/javascript" src="/modules/guest/home/home.module.js"></script>
		<script type="text/javascript" src="/modules/guest/home/home.routes.js"></script>
		<script type="text/javascript" src="/modules/guest/home/controllers/home.controller.js"></script>
		<script type="text/javascript" src="/modules/guest/home/controllers/jobVacancyInfo.controller.js"></script>

		<!-- loginModule files -->
		<script type="text/javascript" src="/modules/login/login.module.js"></script>
		<script type="text/javascript" src="/modules/login/login.routes.js"></script>
		<script type="text/javascript" src="/modules/login/controllers/login.controller.js"></script>

		<!-- guest app base module files -->
		<script type="text/javascript" src="/modules/guest/guest.module.js"></script>
		<script type="text/javascript" src="/modules/guest/guest.routes.js"></script>

	</body>
</html>
