<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/




// Route::get('/', function () {
// 	return view('home');
// });




Route::get('/', function() {
	session_start();
	if(isset($_SESSION['loginInfo'])) {
		return redirect('/admin');
	} else {
		return view('guest');
	}
});


Route::get('/admin', function() {
	session_start();
	if(isset($_SESSION['loginInfo'])) {
		return view('admin');
	} else {
		return redirect('/#/home');
	}
});






// JobVacancyController
Route::post('jobVacancy/enlist', 'JobVacancyController@enlist');
Route::post('jobVacancy/save', 'JobVacancyController@save')->middleware('sessionCheck');
Route::post('jobVacancy/delete', 'JobVacancyController@delete')->middleware('sessionCheck');



// JobVacancyAplicationController
Route::post('jobVacancyApplication/enlist', 'JobVacancyApplicationController@enlist')->middleware('sessionCheck');
Route::post('jobVacancyApplication/save', 'JobVacancyApplicationController@save')->middleware('sessionCheck');
Route::post('jobVacancyApplication/delete', 'JobVacancyApplicationController@delete')->middleware('sessionCheck');



// JobVacancyAplicationStageController
// Route::post('jobVacancyApplicationStage/enlist', 'JobVacancyApplicationStageController@enlist')->middleware('sessionCheck');
Route::post('jobVacancyApplicationStage/save', 'JobVacancyApplicationStageController@save')->middleware('sessionCheck');
Route::post('jobVacancyApplicationStage/delete', 'JobVacancyApplicationStageController@delete')->middleware('sessionCheck');



// CompanyController
Route::post('company/enlist', 'CompanyController@enlist')->middleware('sessionCheck');
Route::post('company/save', 'CompanyController@save')->middleware('sessionCheck');
Route::post('company/delete', 'CompanyController@delete')->middleware('sessionCheck');



// CandidateController
Route::post('candidate/enlist', 'CandidateController@enlist')->middleware('sessionCheck');
Route::post('candidate/save', 'CandidateController@save')->middleware('sessionCheck');
Route::post('candidate/delete', 'CandidateController@delete')->middleware('sessionCheck');
Route::post('candidate/uploadFile', 'CandidateController@uploadFile')->middleware('sessionCheck');
Route::post('candidate/deleteFile', 'CandidateController@deleteFile')->middleware('sessionCheck');



// UserController
Route::post('user/enlist', 'UserController@enlist')->middleware('sessionCheck');
Route::post('user/save', 'UserController@save')->middleware('sessionCheck');
Route::post('user/register', 'UserController@register');
Route::post('user/resetPassword', 'UserController@resetPassword');
Route::post('user/accessCheckEmail', 'UserController@accessCheckEmail');



// MainDashboardController
Route::post('mainDashboard/kpiCount', 'MainDashboardController@kpiCount')->middleware('sessionCheck');



// FeatureController
Route::post('feature/enlist', 'FeatureController@enlist')->middleware('sessionCheck');





// CutomAuthController
Route::post('auth/loginInfo', 'CustomAuthController@loginInfo')->middleware('sessionCheck');
Route::post('auth/login', 'CustomAuthController@login');
Route::post('auth/logout', 'CustomAuthController@logout')->middleware('sessionCheck');
Route::post('auth/accessCheckPassword', 'CustomAuthController@accessCheckPassword');









Route::post('practice/case1', 'PracticeController@case1');




