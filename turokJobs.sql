/*
SQLyog Ultimate v11.33 (64 bit)
MySQL - 10.1.13-MariaDB : Database - turokjobs
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`turokjobs` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `turokjobs`;

/*Table structure for table `candidate_files` */

DROP TABLE IF EXISTS `candidate_files`;

CREATE TABLE `candidate_files` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `candidateId` int(11) DEFAULT NULL,
  `file` text,
  `date` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `candidateId` (`candidateId`),
  CONSTRAINT `candidate_files_ibfk_1` FOREIGN KEY (`candidateId`) REFERENCES `candidates` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `candidate_files` */

/*Table structure for table `candidate_network_profiles` */

DROP TABLE IF EXISTS `candidate_network_profiles`;

CREATE TABLE `candidate_network_profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `candidateId` int(11) DEFAULT NULL,
  `networkName` varchar(255) DEFAULT NULL,
  `link` text,
  PRIMARY KEY (`id`),
  KEY `candidateId` (`candidateId`),
  CONSTRAINT `candidate_network_profiles_ibfk_1` FOREIGN KEY (`candidateId`) REFERENCES `candidates` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=109 DEFAULT CHARSET=latin1;

/*Data for the table `candidate_network_profiles` */

insert  into `candidate_network_profiles`(`id`,`candidateId`,`networkName`,`link`) values (93,3,'LinkedIn','facebook.com/vm.deguzman'),(94,14,'Facebook','facebook.com/ang.pogi.ko.taena.nyo'),(95,14,'Linkedin','linkedin.com/maniti.for.manager'),(107,9,'Facebook','facebok.com/sample.profile'),(108,1,'Facebook','facebook.com/alex.puge.coroza');

/*Table structure for table `candidates` */

DROP TABLE IF EXISTS `candidates`;

CREATE TABLE `candidates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `contact` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `source` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `booleanSearch` text,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

/*Data for the table `candidates` */

insert  into `candidates`(`id`,`name`,`contact`,`email`,`source`,`status`,`booleanSearch`,`created_at`,`updated_at`) values (1,'Alex Coroza','0935 329 3790','coroza.alex.urriza@gmail.com','Facebook','Available',NULL,'2016-09-25 05:53:42','0000-00-00 00:00:00'),(2,'Honey Lyn Tirador','0999 406 8800','honey.tirador@yahoo.com','Facebook','Available',NULL,'2016-09-29 21:24:37','2016-10-08 03:24:07'),(3,'Vanessa De Guzman','09178270612','vm.deguzman@yahoo.com','LinkedIn','Available',NULL,'2016-09-30 12:10:13','0000-00-00 00:00:00'),(9,'Joymecarl De Guzman','09989548301','joyme.deguzman@gmail.com','Facebook','Available',NULL,'2016-10-02 19:42:25','2016-10-03 21:14:06'),(14,'Charliemaine Maniti','09173784614','charlemaine202@gmail.com','Facebook','Available',NULL,'2016-10-05 19:24:22','2016-10-05 19:24:22');

/*Table structure for table `companies` */

DROP TABLE IF EXISTS `companies`;

CREATE TABLE `companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `province` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `postalCode` varchar(255) DEFAULT NULL,
  `contactNumbers` text,
  `websites` text,
  `division` varchar(255) DEFAULT NULL,
  `types` text,
  `pezaRegistered` varchar(255) DEFAULT NULL,
  `otherInfo` text,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `companies` */

insert  into `companies`(`id`,`name`,`address`,`city`,`province`,`country`,`postalCode`,`contactNumbers`,`websites`,`division`,`types`,`pezaRegistered`,`otherInfo`,`created_at`,`updated_at`) values (1,'Globe Telecom','Globe Telecom Plaza','Mandaluyong','Metro Manila','Philipines','1221','8080|09827121222','globe.com.ph',NULL,NULL,NULL,NULL,'2016-09-25 05:55:04','0000-00-00 00:00:00'),(2,'STRATA','Centerpoint Bldg','Pasig','Metro Manila','Philippines','4992','123|9898123','strata.ph',NULL,NULL,NULL,NULL,'2016-09-25 17:25:30','2016-09-25 17:25:30'),(4,'Storm Technologies','Centerpoint Bldg','Pasig','Metro Manila','Philippines','4922','','','Techhr','','Yes','Peter Cauton as CEO and Paolo Dela Fuente as CTO','2016-10-13 02:17:04','2016-10-13 02:44:42'),(6,'Sony Pictures Ph','Sony Tower Inc, BGC','Taguig','Metro Manila','Philippines','9008','8823 7872 62|223 232 12','sony.com.ph|sony.com.jp','Filming','Filming|Fucking','Yes','fake company ata to, fucker','2016-10-13 23:24:06','2016-10-14 00:35:35'),(7,'ISBX','PSE Tower, Ortigas','Pasig','Metro Manila','Philippines','9008','123|321','123.com|321.com|amazingwalkrace.org','Tech','asdasd','Yes','Bumagsak ako dine date','2016-10-14 00:39:52','2016-10-14 00:39:52');

/*Table structure for table `company_contact_persons` */

DROP TABLE IF EXISTS `company_contact_persons`;

CREATE TABLE `company_contact_persons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `companyId` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `position` varchar(255) DEFAULT NULL,
  `contact` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `companyId` (`companyId`),
  CONSTRAINT `company_contact_persons_ibfk_1` FOREIGN KEY (`companyId`) REFERENCES `companies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

/*Data for the table `company_contact_persons` */

insert  into `company_contact_persons`(`id`,`companyId`,`name`,`position`,`contact`,`email`) values (16,2,'Orvin Hilomen',NULL,NULL,NULL),(17,2,'Dino Alcoseba',NULL,NULL,NULL),(18,2,'Peter Cauton',NULL,NULL,NULL),(20,4,'Rach Bachar','Marketing Supervisor','09887266151','rach@stormbenefits.com'),(24,6,'Hishashi Sakamoto','Ceo','098872788712','fucker@fucking.com'),(25,6,'Tomoe','Familiar','0099 92 122','tomoe@kamisama.com'),(26,7,'Asd','Asdasd','123123','asd@asd');

/*Table structure for table `company_notes` */

DROP TABLE IF EXISTS `company_notes`;

CREATE TABLE `company_notes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `companyId` int(11) DEFAULT NULL,
  `note` text,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `companyId` (`companyId`),
  CONSTRAINT `company_notes_ibfk_1` FOREIGN KEY (`companyId`) REFERENCES `companies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

/*Data for the table `company_notes` */

insert  into `company_notes`(`id`,`companyId`,`note`,`date`) values (5,2,'asdjklakls klasdasdjklasdjkl  askldasd klasd ajkl','2016-10-14'),(9,6,'qwe','2016-10-13'),(10,6,'zxc','2016-10-05'),(11,6,'asd','2016-10-13'),(12,7,'qweqw','2016-10-14'),(13,7,'qweqwwwa','2016-10-14'),(14,7,'qweqwewqe','2016-10-14');

/*Table structure for table `features` */

DROP TABLE IF EXISTS `features`;

CREATE TABLE `features` (
  `feature` varchar(255) NOT NULL,
  PRIMARY KEY (`feature`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `features` */

insert  into `features`(`feature`) values ('Candidates'),('Candidates >> Add'),('Candidates >> Add Files'),('Candidates >> Delete'),('Candidates >> Edit'),('Candidates >> Info'),('Candidates >> Search'),('Companies'),('Companies >> Add'),('Companies >> Delete'),('Companies >> Edit'),('Companies >> Info'),('Companies >> Search'),('Job Vacancies'),('Job Vacancies >> Add'),('Job Vacancies >> Delete'),('Job Vacancies >> Edit'),('Job Vacancies >> Info'),('Job Vacancies >> Search'),('Job Vacancy Application Stages >> Add'),('Job Vacancy Application Stages >> Delete'),('Job Vacancy Application Stages >> Edit'),('Job Vacancy Applications >> Add'),('Job Vacancy Applications >> Delete'),('Job Vacancy Applications >> Edit'),('Main Dashboard'),('Main Dashboard >> Select Recruiter'),('Profile'),('Profile >> Change Password'),('Recruiters'),('Recruiters >> Access Management'),('Recruiters >> Add'),('Recruiters >> Delete'),('Recruiters >> Edit'),('Recruiters >> Info'),('Recruiters >> Reset Password'),('Recruiters >> Search');

/*Table structure for table `job_vacancies` */

DROP TABLE IF EXISTS `job_vacancies`;

CREATE TABLE `job_vacancies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `companyId` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `slots` int(11) DEFAULT NULL,
  `requirements` text,
  `overview` text,
  `description` text,
  `positionLevel` varchar(255) DEFAULT NULL,
  `priorityLevel` int(11) DEFAULT NULL,
  `salaryRange` int(11) DEFAULT NULL,
  `placementPercent` int(11) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `remarks` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `companyId` (`companyId`),
  CONSTRAINT `job_vacancies_ibfk_1` FOREIGN KEY (`companyId`) REFERENCES `companies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

/*Data for the table `job_vacancies` */

insert  into `job_vacancies`(`id`,`companyId`,`title`,`slots`,`requirements`,`overview`,`description`,`positionLevel`,`priorityLevel`,`salaryRange`,`placementPercent`,`status`,`remarks`,`created_at`,`updated_at`) values (1,1,'PHP Developer',11,'Malake ang muscles|Dapat kulay brown|Sanay sa matigas na keyboard|mala Green Mamba ang galawan','Lorem ipsum keme keme keme 48 years boyband krang-krang.','Lorem ipsum keme keme keme 48 years boyband krang-krang at nang na ang chopopo at nang cheapangga warla chopopo at bakit.','Hokage level',2,100000,14,'Active','Mukang mabilis lang tayo makakanap kase marameng hokage dito sa kamaynilaan','2016-09-21 23:38:44','2016-09-27 20:21:04'),(2,1,'Leader Ng Konoha West',23,'pogi|macho','Be a badass hokage! We need you now!','Mandadakot ng .....','hokage',1,54000,3,'Active','fuck that','2016-09-27 15:34:11','2016-09-29 13:38:35'),(3,1,'Forklift Driver',1,'macho|GUAPO|pogi|mala  COCO MARTIN','tiwala lang, ok din to','Transport and move items such as gadgets, appliances inside our warehouse. stack them based on our company rules','Staff Production',5,15000,10,'Active','G lang ng G','2016-09-28 00:00:00','2016-11-03 01:19:24'),(19,1,'Messenger And Delivery',1,'sing macho ni zanjoe marudo|sing pogi ni coco martin|mabilis maglakad','Some overiew here','Some long and very very very very very long description here. Lorem ipsum solor sit amet','Hokage Level',3,15000,9,'Active','Some very very very very evry very logn remark here. this must be important.','2016-08-15 00:00:00','2016-10-07 15:09:19');

/*Table structure for table `job_vacancy_application_stages` */

DROP TABLE IF EXISTS `job_vacancy_application_stages`;

CREATE TABLE `job_vacancy_application_stages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jobVacancyApplicationId` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `stage` varchar(255) DEFAULT NULL,
  `result` varchar(255) DEFAULT NULL,
  `notes` text,
  PRIMARY KEY (`id`),
  KEY `jobVacancyApplicationId` (`jobVacancyApplicationId`),
  CONSTRAINT `job_vacancy_application_stages_ibfk_1` FOREIGN KEY (`jobVacancyApplicationId`) REFERENCES `job_vacancy_applications` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=109 DEFAULT CHARSET=latin1;

/*Data for the table `job_vacancy_application_stages` */

insert  into `job_vacancy_application_stages`(`id`,`jobVacancyApplicationId`,`date`,`stage`,`result`,`notes`) values (84,75,'2016-10-12','1st Interview','Pending','Biglang nagkaroon ng opening. Dumating na daw ung mga investors galing Russia.'),(85,75,'2016-10-15','Job Offer','Pending','The moment of truth has come!'),(86,77,'2016-10-14','Other','Pending','asdqe1e 344 asd as'),(99,100,'2016-10-06','CV Sent','Pending','Lorem Ipsum dolor sit amet'),(104,105,'2016-09-15','1st Interview','Success','Llamado to. Mukhang mkakamoney shot ako dito ah.'),(105,105,'2016-09-17','2nd Interview','Success','Hmmm...'),(106,106,'2016-10-07','CV Sent','Pending','taenang notes'),(108,107,'2016-11-02','Other','Pending','Titingnan kung tunay ang pinagsasabe sa phone interview.');

/*Table structure for table `job_vacancy_applications` */

DROP TABLE IF EXISTS `job_vacancy_applications`;

CREATE TABLE `job_vacancy_applications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) DEFAULT NULL,
  `jobVacancyId` int(11) DEFAULT NULL,
  `candidateId` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `dateClosed` date DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `candidateId` (`candidateId`),
  KEY `jobVacancyId` (`jobVacancyId`),
  KEY `userId` (`userId`),
  CONSTRAINT `job_vacancy_applications_ibfk_1` FOREIGN KEY (`candidateId`) REFERENCES `candidates` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `job_vacancy_applications_ibfk_2` FOREIGN KEY (`jobVacancyId`) REFERENCES `job_vacancies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `job_vacancy_applications_ibfk_3` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=108 DEFAULT CHARSET=latin1;

/*Data for the table `job_vacancy_applications` */

insert  into `job_vacancy_applications`(`id`,`userId`,`jobVacancyId`,`candidateId`,`date`,`dateClosed`,`status`) values (73,4,2,3,'2016-10-08',NULL,'Active'),(75,3,1,14,'2016-09-11',NULL,'Active'),(76,3,2,14,'2016-09-11',NULL,'Active'),(77,3,3,14,'2016-10-07',NULL,'Active'),(100,3,1,9,'2016-10-05',NULL,'Active'),(101,3,19,9,'2016-10-06',NULL,'Active'),(105,4,1,1,'2016-10-05',NULL,'Active'),(106,4,3,1,'2016-10-05',NULL,'Active'),(107,5,19,1,'2016-10-31',NULL,'Active');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `features` text,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`email`,`password`,`salt`,`type`,`features`,`created_at`,`updated_at`) values (3,'Alex Coroza','coroza.alex.urriza@gmail.com','726495e5bce05c51cca1d0bc3c3fafaeb9dc4547','4ko9Ygksble9Tq4OQPiyfpUWO','Admin','Candidates|Candidates >> Add|Candidates >> Add Files|Candidates >> Delete|Candidates >> Edit|Candidates >> Info|Candidates >> Search|Companies|Companies >> Add|Companies >> Delete|Companies >> Edit|Companies >> Info|Companies >> Search|Job Vacancies|Job Vacancies >> Add|Job Vacancies >> Delete|Job Vacancies >> Edit|Job Vacancies >> Info|Job Vacancies >> Search|Job Vacancy Application Stages >> Add|Job Vacancy Application Stages >> Delete|Job Vacancy Application Stages >> Edit|Job Vacancy Applications >> Add|Job Vacancy Applications >> Delete|Job Vacancy Applications >> Edit|Main Dashboard|Main Dashboard >> Select Recruiter|Profile|Profile >> Change Password|Recruiters|Recruiters >> Add|Recruiters >> Delete|Recruiters >> Edit|Recruiters >> Info|Recruiters >> Reset Password|Recruiters >> Search|Recruiters >> Access Management','2016-09-14 16:51:48','2016-11-03 13:57:53'),(4,'Kirby Pontipedra','kirby.pontipedra@gcgconsulting.com','4c63613a9abe8d9486bd7827b83f3a5cf11d5edd','jYE7ZbMxv9ikPsVOrdTRi1SOR','Recruiter','Candidates|Candidates >> Add|Candidates >> Add Files|Candidates >> Delete|Candidates >> Edit|Candidates >> Info|Candidates >> Search|Companies|Companies >> Add|Companies >> Delete|Companies >> Edit|Companies >> Info|Companies >> Search|Job Vacancies|Job Vacancies >> Add|Job Vacancies >> Delete|Job Vacancies >> Edit|Job Vacancies >> Info|Job Vacancies >> Search|Job Vacancy Application Stages >> Add|Job Vacancy Application Stages >> Delete|Job Vacancy Application Stages >> Edit|Job Vacancy Applications >> Add|Job Vacancy Applications >> Delete|Job Vacancy Applications >> Edit|Main Dashboard|Main Dashboard >> Select Recruiter|Profile|Profile >> Change Password|Recruiters|Recruiters >> Add|Recruiters >> Delete|Recruiters >> Edit|Recruiters >> Info|Recruiters >> Reset Password|Recruiters >> Search|Recruiters >> Access Management','2016-10-11 00:34:51','2016-10-11 00:34:51'),(5,'Venus Lloveras Langa','venus.chan@yahoo.com.ph','654e87d11ee5debed2fda82902f4ee374c2abaa7','w5EQKshB4o1dRmXTXkFnHLgSV','Recruiter','Profile|Profile >> Change Password|Recruiters|Recruiters >> Reset Password|Recruiters >> Search','2016-10-17 23:24:11','2016-11-03 00:35:22');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
